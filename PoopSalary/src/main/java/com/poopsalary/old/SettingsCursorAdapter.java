package com.poopsalary;

import com.poopsalary.R;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class SettingsCursorAdapter extends SimpleCursorAdapter
{
    private int layout;
    protected ListView mListView;

    protected static class RowViewHolder {
        public TextView mTitle;
        public TextView mText;
    }

    public SettingsCursorAdapter (Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        this.layout = layout;
        context.getApplicationInfo();

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {

        Cursor c = getCursor();

        final LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(layout, parent, false);

        String profileName = c.getString(c.getColumnIndexOrThrow("profileName"));
        String wageType = c.getString(c.getColumnIndexOrThrow("wageType"));
        double wage = c.getDouble(c.getColumnIndexOrThrow("wage"));
        /**
         * Next set the name of the entry.
         */

        TextView tvProfileName = (TextView) v.findViewById(R.id.settingsprofilename);
        TextView tvWageType = (TextView) v.findViewById(R.id.settingswagetype);
        TextView tvWage = (TextView) v.findViewById(R.id.settingswage);

        tvProfileName.setText(profileName);
        tvWageType.setText(wageType);
        tvWage.setText("$" + Double.toString(wage));

        return v;
    }

    @Override
    public void bindView(View v, Context context, Cursor c) {

        String profileName = c.getString(c.getColumnIndexOrThrow("profileName"));
        String wageType = c.getString(c.getColumnIndexOrThrow("wageType"));
        double wage = c.getDouble(c.getColumnIndexOrThrow("wage"));
        /**
         * Next set the name of the entry.
         */

        TextView tvProfileName = (TextView) v.findViewById(R.id.settingsprofilename);
        TextView tvWageType = (TextView) v.findViewById(R.id.settingswagetype);
        TextView tvWage = (TextView) v.findViewById(R.id.settingswage);

        tvProfileName.setText(profileName);
        tvWageType.setText(wageType);
        tvWage.setText("$" + Double.toString(wage));
    }

}