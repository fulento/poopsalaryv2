package com.poopsalary.old;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import com.poopsalary.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class StatsCursorAdapter extends SimpleCursorAdapter
{
    private int layout;
    protected ListView mListView;
    private Currency curr  = Currency.getInstance(Locale.getDefault());
    private SharedPreferences currPref;

    protected static class RowViewHolder {
        public TextView mTitle;
        public TextView mText;
    }

    public StatsCursorAdapter (Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        currPref=context.getSharedPreferences("PoopSalaryPreferences", Context.MODE_PRIVATE);

        this.layout = layout;
        context.getApplicationInfo();

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        Cursor c = getCursor();

        final LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(layout, parent, false);

        String dateEntered = c.getString(c.getColumnIndex("dateEntered"));
        String timeElapsed = c.getString(c.getColumnIndex("timeElapsed"));
        double moneyEarned = c.getDouble(c.getColumnIndex("moneyEarned"));
        /**
         * Next set the name of the entry.
         */

        TextView tvDateEntered = (TextView) v.findViewById(R.id.dateentered);
        TextView tvTimeElapsed = (TextView) v.findViewById(R.id.timeelapsed);
        TextView tvMoneyEarned = (TextView) v.findViewById(R.id.moneyearned);

        tvDateEntered.setText(dateEntered);
        tvTimeElapsed.setText(timeElapsed);
        tvMoneyEarned.setText(curr.getSymbol() + Double.toString(ConvertWageToLocale(moneyEarned)));

        return v;
    }

    @Override
    public void bindView(View v, Context context, Cursor c) {

        String dateEntered = intDateToString(c.getInt(c.getColumnIndex("dateEntered")));
        String timeElapsed = c.getString(c.getColumnIndex("timeElapsed"));
        double moneyEarned = c.getDouble(c.getColumnIndex("moneyEarned"));
        /**
         * Next set the name of the entry.
         */

        TextView tvDateEntered = (TextView) v.findViewById(R.id.dateentered);
        TextView tvTimeElapsed = (TextView) v.findViewById(R.id.timeelapsed);
        TextView tvMoneyEarned = (TextView) v.findViewById(R.id.moneyearned);

        tvDateEntered.setText(dateEntered);
        tvTimeElapsed.setText(formatTimeToString(timeElapsed));
        tvMoneyEarned.setText(curr.getSymbol() + Double.toString(ConvertWageToLocale(moneyEarned)));
    }

    private String formatTimeToString(String timeElapsed)
    {
        long seconds = Long.parseLong(timeElapsed);
        long minutes = seconds/60;
        String formattedString;

        if(minutes > 9)
        {
            formattedString = String.valueOf(minutes);
        }
        else
        {
            formattedString = "0" + String.valueOf(minutes);
        }

        formattedString += ":";

        if(seconds % 60 > 9)
        {
            formattedString += String.valueOf(seconds % 60);
        }
        else
        {
            formattedString += "0" + String.valueOf(seconds % 60);
        }
        return formattedString;

    }

    private double ConvertWageToLocale(double earnings)
    {
        return  round(earnings * currPref.getFloat("ConversionRate", 1), 2, 1);
    }

    public static double round(double unrounded, int precision, int roundingMode)
    {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }

    private String intDateToString(int date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String dateString = formatter.format(new Date(date*1000L));
        return dateString;
    }
}