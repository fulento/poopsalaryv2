package com.poopsalary.old;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;

public class DatabaseHelper extends SQLiteOpenHelper
{
    private static final String DB_NAME = "PoopSalary";

    //previous version 12. deleted stats table to data integrity
    private static final int DATABASE_VERSION = 16;

    //create database tables
    private static final String SQL_CREATETABLE_SETTINGS = "CREATE TABLE Settings (_id integer primary key autoincrement, wageType text, wage numeric);";
    private static final String SQL_CREATETABLE_STATS = "CREATE TABLE Stats (_id integer primary key autoincrement, dateEntered integer, timeElapsed integer, moneyEarned numeric, ratings numeric);";
    private static final String SQL_CREATETABLE_CURRENTPOOP = "CREATE TABLE CurrentPoop (_id integer primary key autoincrement, poopStart numeric)";
    private static final String SQL_CREATETABLE_PROFILES = "CREATE TABLE Profiles (_id integer primary key autoincrement, profileName text, wageType text, wage numeric, beingUsed text);";
    private static final String SQL_CREATETABLE_CURRENTPROFILE = "CREATE TABLE CurrentProfile (_id integer primary key autoincrement, profileId integer);";

    public DatabaseHelper(Context context)
    {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database)
    {
        database.execSQL(SQL_CREATETABLE_SETTINGS);
        database.execSQL(SQL_CREATETABLE_STATS);
        database.execSQL(SQL_CREATETABLE_CURRENTPOOP);
        database.execSQL(SQL_CREATETABLE_PROFILES);
        database.execSQL(SQL_CREATETABLE_CURRENTPROFILE);

        database.insert("Profiles", null, createProfile("Default", 10, "Hourly"));
        Cursor c = database.rawQuery("Select _id FROM Profiles", null);

        c.moveToFirst();

        if(c.getCount() > 0)
        {
            database.insert("CurrentProfile", null, setCurrentProfileContentValues(c.getInt(c.getColumnIndex("_id"))));
        }

        database.insert("Profiles", null, createProfile("Overtime", 10, "Hourly"));
        database.insert("Profiles", null, createProfile("2nd Job", 10, "Hourly"));
        //database.insert("Stats", null, createStats(new Date().getTime(), 100000, 4.5));
        //database.insert("Stats", null, createStats(new Date().getTime() + 1000, 200000, 5.5));
        //database.insert("Stats", null, createStats(new Date().getTime() + 1000, 300000, 6.5));
    }

    // Method is called during an upgrade of the database, e.g. if you increase
    // the database version
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
    {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        Cursor c = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='CurrentPoop'", null);

        if(c.getCount() == 0)
        {
            database.execSQL(SQL_CREATETABLE_CURRENTPOOP);
        }
        c.close();

        c = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='Stats'", null);

        if(c.getCount() == 0)
        {
            database.execSQL(SQL_CREATETABLE_STATS);
        }
        c.close();

        c = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='Settings'", null);

        if(c.getCount() == 0)
        {
            database.execSQL(SQL_CREATETABLE_SETTINGS);
        }
        c.close();

        c = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='Profiles'", null);

        if(c.getCount() == 0)
        {
            database.execSQL(SQL_CREATETABLE_PROFILES);
            database.execSQL(SQL_CREATETABLE_CURRENTPROFILE);

            ContentValues initialValues = new ContentValues();
            initialValues.put("profileName", "Default");
            initialValues.put("wage", "Overtime");
            initialValues.put("wageType", "Hourly");
            database.insert("Profiles", null, createProfile("Default", 10, "Hourly"));
            database.insert("Profiles", null, createProfile("Overtime", 10, "Hourly"));
            database.insert("Profiles", null, createProfile("2nd Job", 10, "Hourly"));
        }
        c.close();

        Cursor p = database.rawQuery("Select _id FROM Profiles", null);

        p.moveToFirst();

        if(p.getCount() > 0)
        {
            database.insert("CurrentProfile", null, setCurrentProfileContentValues(p.getInt(p.getColumnIndex("_id"))));
        }
        else
        {
            database.insert("CurrentProfile", null, setCurrentProfileContentValues(1));
        }

        c.close();
    }

    private ContentValues setCurrentProfileContentValues(int _id)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put("profileId", _id);

        return initialValues;
    }

    public ContentValues createProfile(String profileName, long wage, String wageType)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put("profileName", profileName);
        initialValues.put("wage", wage);
        initialValues.put("wageType", wageType);
        return initialValues;
    }

    public ContentValues createStats(long dateEntered, long timeElapsed, double moneyEarned)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put("dateEntered", dateEntered/1000);
        initialValues.put("timeElapsed", timeElapsed);
        initialValues.put("moneyEarned", moneyEarned);

        return initialValues;
    }
}