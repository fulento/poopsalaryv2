package com.poopsalary.old;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;

public class DatabaseAdapter {

    // Database fields
    public static final String KEY_ROWID = "_id";
    public static final String KEY_CHILDNAME = "childName";
    public static final String KEY_EMAIL = "email";
    private Context context;
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public DatabaseAdapter(Context context)
    {
        this.context = context;
    }

    public DatabaseAdapter open() throws SQLException
    {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close()
    {
        dbHelper.close();
    }

    /**
     * Deletes a Child
     */
    public boolean deleteStat(long rowId)
    {
        return database.delete("Stats", KEY_ROWID + "=" + rowId, null) > 0;
    }

    public void createStats(long dateEntered, long timeElapsed, double moneyEarned)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put("dateEntered", dateEntered/1000);
        initialValues.put("timeElapsed", timeElapsed);
        initialValues.put("moneyEarned", moneyEarned);
        database.insert("Stats", null, initialValues);
    }

    /**
     * Return a Cursor over the list of all children in the database
     *
     * @return Cursor over all children
     */
    public Cursor fetchStatsByDate()
    {
        String sqlStr = "SELECT _id, dateEntered, timeElapsed, moneyEarned FROM Stats ORDER BY dateEntered DESC";

        return database.rawQuery(sqlStr, null);

    }

    public Cursor fetchStatsByTime()
    {
        String sqlStr = "SELECT _id, dateEntered, timeElapsed, moneyEarned FROM Stats ORDER BY timeElapsed DESC";

        return database.rawQuery(sqlStr, null);

    }

    public Cursor fetchStatsByMoney()
    {
        String sqlStr = "SELECT _id, dateEntered, timeElapsed, moneyEarned FROM Stats ORDER BY moneyEarned DESC";

        return database.rawQuery(sqlStr, null);

    }

    public Cursor fetchChartStats()
    {
        String sqlStr = "SELECT TOP 5 _id, dateEntered, timeElapsed, moneyEarned FROM Stats ORDER BY _id ASC";

        return database.rawQuery(sqlStr, null);

    }

    public String fetchStatById(long id)
    {
        String sqlStr = "SELECT _id, dateEntered, timeElapsed, moneyEarned FROM Stats WHERE _id = " + id;

        Cursor c = database.rawQuery(sqlStr, null);
        Currency curr  = Currency.getInstance(Locale.getDefault());

        String tElapsed;
        double mEarned;
        String shareString;

        c.moveToFirst();

        if(c.getCount() > 0)
        {
            tElapsed = formatTimeToString(c.getString(c.getColumnIndex("timeElapsed")));
            mEarned = c.getDouble(c.getColumnIndex("moneyEarned"));
            shareString = "I made " + curr.getSymbol() + Double.toString(ConvertWageToLocale(mEarned)) + " in " + tElapsed + " minute(s) using #PoopSalary";
        }
        else
            shareString = "No stat found for id: " + id;

        return shareString;
    }
    public String fetchProfileById(long id)
    {
        String sqlStr = "SELECT _id, profileName, wageType, wage FROM Profiles WHERE _id =" + id;
        Cursor c = database.rawQuery(sqlStr, null);
        String profileString;

        c.moveToFirst();
        Currency curr = Currency.getInstance(Locale.getDefault());

        if(c.getCount() > 0)
        {
            profileString = c.getString(c.getColumnIndex("profileName"));
            profileString += " Wage: " + curr.getSymbol();
            profileString += c.getString(c.getColumnIndex("wage"));
            profileString += "/";
            profileString += c.getString(c.getColumnIndex("wageType"));

        }
        else
            profileString = "You have not selected a profile. Select settings below";

        return profileString;
    }

    public Cursor fetchProfiles()
    {
        String sqlStr = "SELECT _id, profileName, wageType, wage FROM Profiles";

        return database.rawQuery(sqlStr, null);
    }

    public long fetchPoopStartTime()
    {
        String sqlStr = "SELECT _id, poopStart FROM CurrentPoop ORDER BY _id ASC";

        Cursor c = database.rawQuery(sqlStr, null);
        long startTime;

        c.moveToFirst();

        if(c.getCount() > 0)
        {
            startTime = c.getLong(c.getColumnIndex("poopStart"));
        }
        else
            startTime = SystemClock.elapsedRealtime();

        return startTime;

    }

    public boolean doesPoopExist()
    {
        String mySql = "SELECT * FROM CurrentPoop";
        Cursor c = database.rawQuery(mySql,null);

        c.moveToFirst();

        if(c.getCount() == 0)
        {
            return false;
        }
        else
            return true;
    }


    public boolean doSettingsExist()
    {
        String mySql = "SELECT * FROM Profiles";
        Cursor c = database.rawQuery(mySql,null);

        c.moveToFirst();

        if(c.getCount() == 0)
        {
            return false;
        }
        else
            return true;
    }

    public boolean isHighTimeScore(long timeElapsed)
    {
        String sqlStr = "SELECT _id, dateEntered, timeElapsed, moneyEarned FROM Stats ORDER BY _id LIMIT 1";

        Cursor c = database.rawQuery(sqlStr, null);

        c.moveToFirst();

        if(c.getCount() > 0)
        {
            if(timeElapsed >= c.getDouble(c.getColumnIndex("timeElapsed")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public boolean isHighDollarScore(double dollarAmount)
    {
        String sqlStr = "SELECT _id, dateEntered, timeElapsed, moneyEarned FROM Stats ORDER BY moneyEarned DESC LIMIT 1";

        Cursor c = database.rawQuery(sqlStr, null);

        c.moveToFirst();

        if(c.getCount() > 0)
        {
            if(dollarAmount > c.getDouble(c.getColumnIndex("moneyEarned")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public Cursor fetchSettings(long currentProfile)
    {
        String sqlStr = "SELECT _id , profileName, wageType, wage FROM Profiles WHERE _id = " + currentProfile;

        return database.rawQuery(sqlStr, null);
    }

    public int getCurrentProfile()
    {
        String sqlStr = "SELECT profileId FROM CurrentProfile";
        int currentId = 0;

        Cursor c = database.rawQuery(sqlStr, null);

        c.moveToFirst();

        if(c.getCount()>0)
        {
            currentId = c.getInt(c.getColumnIndex("profileId"));
        }
        else
        {
            updateCurrentProfile(1);
            currentId=1;
        }

        return currentId;
    }

    public void updateCurrentProfile(long id)
    {
        String mySql = "UPDATE CurrentProfile SET profileId = '" + id + "'";

        database.execSQL(mySql);
    }

    public void createPoopTime(long poopTime)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put("poopStart", poopTime);
        database.insert("CurrentPoop", null, initialValues);
    }

    public void deletePoopTime()
    {
        //database.execSQL("DELETE CurrentPoop");
        database.delete("CurrentPoop", null, null);
    }

    public void deleteAllJobs()
    {
        database.delete("Profiles", null, null);
    }

    public void deleteAllStats()
    {
        database.delete("Stats", null, null);
    }

    public void deleteCurrentProfile()
    {
        database.delete("CurrentProfile", null, null);
    }

    public void deleteCurrentPoop()
    {
        database.delete("CurrentPoop", null, null);
    }

    public void deleteSettings()
    {
        database.delete("Settings", null, null);
    }


    public void updateSettings(long profileId, String wageType, double wage)
    {
        String mySql = "UPDATE Profiles SET wageType = '" + wageType + "', wage = '" + wage + "' WHERE _id = " + profileId;

        database.execSQL(mySql);

    }

    public String getTotalTimeWeek()
    {
        //1 * 24 * 60 * 60 * 1000 = 1 day in milliseconds
        Date newDate = new Date(System.currentTimeMillis() - (7*24*60*60*1000L));
        long dateMilliseconds = Date_to_MilliSeconds(newDate.getDay(), newDate.getMonth(), newDate.getYear(), newDate.getHours(), newDate.getMinutes());

        Cursor c = database.rawQuery("SELECT SUM(timeElapsed) as timeElapsed FROM Stats WHERE dateEntered >= " + dateMilliseconds, null);
        long totalTime = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalTime = c.getLong(c.getColumnIndex("timeElapsed"));
        }
        c.close();

        return formatTime(totalTime);
    }

    public String getTotalTimeMonth()
    {
        //1 * 24 * 60 * 60 * 1000 = 1 day in milliseconds
        Date newDate = new Date(System.currentTimeMillis() - (30*24*60*60*1000L));
        long dateMilliseconds = Date_to_MilliSeconds(newDate.getDay(), newDate.getMonth(), newDate.getYear(), newDate.getHours(), newDate.getMinutes());

        Cursor c = database.rawQuery("SELECT SUM(timeElapsed) as timeElapsed FROM Stats WHERE dateEntered >= " + dateMilliseconds, null);
        long totalTime = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalTime = c.getLong(c.getColumnIndex("timeElapsed"));
        }
        c.close();

        return formatTime(totalTime);
    }

    public String getTotalTimeYtd()
    {
        Cursor c = database.rawQuery("SELECT SUM(timeElapsed) as timeElapsed FROM Stats", null);
        long totalTime = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalTime = c.getLong(c.getColumnIndex("timeElapsed"));
        }
        c.close();

        return formatTime(totalTime);
    }

    public double getTotalEarnedWeek()
    {
        //1 * 24 * 60 * 60 * 1000 = 1 day in milliseconds
        Date newDate = new Date(System.currentTimeMillis() - (7*24*60*60*1000L));
        long dateMilliseconds = Date_to_MilliSeconds(newDate.getDay(), newDate.getMonth(), newDate.getYear(), newDate.getHours(), newDate.getMinutes());

        Cursor c = database.rawQuery("SELECT SUM(moneyEarned) as moneyEarned FROM Stats WHERE dateEntered >= " + dateMilliseconds, null);
        double totalEarned = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalEarned = c.getDouble(c.getColumnIndex("moneyEarned"));
        }
        c.close();

        return totalEarned;

    }

    public double getTotalEarnedMonth()
    {
        //1 * 24 * 60 * 60 * 1000 = 1 day in milliseconds
        Date newDate = new Date(System.currentTimeMillis() - (30*24*60*60*1000L));
        long dateMilliseconds = Date_to_MilliSeconds(newDate.getDay(), newDate.getMonth(), newDate.getYear(), newDate.getHours(), newDate.getMinutes());

        Cursor c = database.rawQuery("SELECT SUM(moneyEarned) as moneyEarned FROM Stats WHERE dateEntered >= " + dateMilliseconds, null);
        double totalEarned = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalEarned = c.getDouble(c.getColumnIndex("moneyEarned"));
        }
        c.close();

        return totalEarned;
    }

    public double getTotalEarnedYtd()
    {
        Cursor c = database.rawQuery("SELECT SUM(moneyEarned) as moneyEarned FROM Stats", null);
        double totalEarned = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalEarned = c.getDouble(c.getColumnIndex("moneyEarned"));
        }
        c.close();

        return totalEarned;
    }

    public double getHighScoreEarned()
    {
        Cursor c = database.rawQuery("SELECT moneyEarned FROM Stats ORDER BY moneyEarned DESC LIMIT 1", null);
        double totalEarned = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalEarned = c.getDouble(c.getColumnIndex("moneyEarned"));
        }
        c.close();

        return totalEarned;
    }

    public String getHighScoreTimed()
    {
        Cursor c = database.rawQuery("SELECT timeElapsed FROM Stats ORDER BY timeElapsed DESC LIMIT 1", null);
        long totalTime = 0;

        if(c.getCount()>0)
        {
            c.moveToFirst();
            totalTime = c.getLong(c.getColumnIndex("timeElapsed"));
        }
        c.close();

        return formatTime(totalTime);
    }

    public String getDate()
    {

        //   Allocates a Date object and initializes it so that it represents the time
        // at which it was allocated, measured to the nearest millisecond.
        Date dateNow = new Date ();

        SimpleDateFormat dateformatMMDDYYYY = new SimpleDateFormat("MM/dd/yyyy");
        String nowMMDDYYYY = dateformatMMDDYYYY.format( dateNow );

        return nowMMDDYYYY;
    }

    private String formatTime(long seconds)
    {
        long minutes = seconds/60;
        String formattedString;

        if(minutes > 9)
        {
            formattedString = String.valueOf(minutes);
        }
        else
        {
            formattedString = "0" + String.valueOf(minutes);
        }

        formattedString += ":";

        if(seconds % 60 > 9)
        {
            formattedString += String.valueOf(seconds % 60);
        }
        else
        {
            formattedString += "0" + String.valueOf(seconds % 60);
        }
        return formattedString;
    }

    public long Date_to_MilliSeconds(int day, int month, int year, int hour, int minute)
    {

        Calendar c = Calendar.getInstance();
        c.set(year, month, day, hour, minute, 00);

        return c.getTimeInMillis();

    }

    private String formatTimeToString(String timeElapsed)
    {
        long seconds = Long.parseLong(timeElapsed);
        long minutes = seconds/60;
        String formattedString;

        if(minutes > 9)
        {;
            formattedString = String.valueOf(minutes);
        }
        else
        {
            formattedString = "0" + String.valueOf(minutes);
        }

        formattedString += ":";

        if(seconds % 60 > 9)
        {
            formattedString += String.valueOf(seconds % 60);
        }
        else
        {
            formattedString += "0" + String.valueOf(seconds % 60);
        }
        return formattedString;

    }

    private double ConvertWageToLocale(double earnings)
    {
        SharedPreferences currPref;
        currPref=context.getSharedPreferences("PoopSalaryPreferences", Context.MODE_PRIVATE);
        return  round(earnings * currPref.getFloat("ConversionRate", 1), 2, 1);
    }

    public static double round(double unrounded, int precision, int roundingMode)
    {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }
}