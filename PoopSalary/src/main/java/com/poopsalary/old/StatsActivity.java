package com.poopsalary.old;

import com.poopsalary.R;
import com.poopsalary.utils.Utilities;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class StatsActivity extends ListActivity {

    private DatabaseAdapter dbHelper;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.old_stats_activity);

        List<String> items = new ArrayList<>();
        String rowText;

        setTitle("Sync Old Stats");

        this.getListView().setDividerHeight(2);
        dbHelper = new DatabaseAdapter(this);
        dbHelper.open();

        Cursor c;

        c = dbHelper.fetchStatsByDate();

        c.moveToFirst();

        if(c.getCount() > 0)
        {
            do
            {
                rowText = Utilities.getFullFormattedDate(c.getLong(c.getColumnIndexOrThrow("dateEntered")) * 1000);
                rowText += " : " + Utilities.MillisecondsToFormattedElapsedTime(c.getLong(c.getColumnIndexOrThrow("timeElapsed")));
                rowText += " : " + Double.toString(c.getDouble(c.getColumnIndexOrThrow("moneyEarned")));
                items.add(rowText);
            }while(c.moveToNext());

        }

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        setListAdapter(itemsAdapter);

        dbHelper.close();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (dbHelper != null)
        {
            dbHelper.close();
        }
    }
}