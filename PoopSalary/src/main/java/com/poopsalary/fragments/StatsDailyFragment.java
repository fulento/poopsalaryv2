package com.poopsalary.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.poopsalary.R;
import com.poopsalary.adapters.DailyStatsAdapter;
import com.poopsalary.adapters.LogsParseQueryAdapter;
import com.poopsalary.models.Logs;
import com.poopsalary.models.StatsByHolder;
import com.poopsalary.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

public class StatsDailyFragment extends ListFragment {

    private Dialog mDialog;
    View mView;
    ArrayList<StatsByHolder> mListStats;
    DailyStatsAdapter mAdapter;
    ListView lvStats;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.list_simple, container, false);
        lvStats = (ListView)mView.findViewById(android.R.id.list);

        showProgressBar("Compiling Daily Stats...");

        ParseQuery<Logs> qYear = new ParseQuery<Logs>(Logs.class);
        qYear.fromLocalDatastore();
        qYear.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                long timeMonday = 0;
                long timeTuesday = 0;
                long timeWednesday = 0;
                long timeThursday = 0;
                long timeFriday = 0;
                long timeSaturday = 0;
                long timeSunday = 0;
                double earnedMonday = 0;
                double earnedTuesday = 0;
                double earnedWednesday = 0;
                double earnedThursday = 0;
                double earnedFriday = 0;
                double earnedSaturday = 0;
                double earnedSunday = 0;

                String dayOfWeek = "";

                //populate data for list
                for(int i = 0; i < logs.size(); i++)
                {
                    dayOfWeek = Utilities.getDayOfWeek(logs.get(i).getTimeCompleted().getTime());

                    switch (dayOfWeek)
                    {
                        case "Monday":
                            timeMonday += logs.get(i).getTotalSeconds();
                            earnedMonday += logs.get(i).getMoneyEarned();
                            break;
                        case "Tuesday":
                            timeTuesday += logs.get(i).getTotalSeconds();
                            earnedTuesday += logs.get(i).getMoneyEarned();
                            break;
                        case "Wednesday":
                            timeWednesday += logs.get(i).getTotalSeconds();
                            earnedWednesday += logs.get(i).getMoneyEarned();
                            break;
                        case "Thursday":
                            timeThursday += logs.get(i).getTotalSeconds();
                            earnedThursday += logs.get(i).getMoneyEarned();
                            break;
                        case "Friday":
                            timeFriday += logs.get(i).getTotalSeconds();
                            earnedFriday += logs.get(i).getMoneyEarned();
                            break;
                        case "Saturday":
                            timeSaturday += logs.get(i).getTotalSeconds();
                            earnedSaturday += logs.get(i).getMoneyEarned();
                            break;
                        case "Sunday":
                            timeSunday += logs.get(i).getTotalSeconds();
                            earnedSunday += logs.get(i).getMoneyEarned();
                            break;
                    }
                }

                mListStats = new ArrayList<StatsByHolder>();
                mListStats.add(new StatsByHolder("DAY", "TIME", "EARNED"));
                mListStats.add(new StatsByHolder("Monday", Utilities.MillisecondsToFormattedElapsedTime(timeMonday *1000), Utilities.ConvertWageToLocale(getActivity(), earnedMonday)));
                mListStats.add(new StatsByHolder("Tuesday", Utilities.MillisecondsToFormattedElapsedTime(timeTuesday *1000), Utilities.ConvertWageToLocale(getActivity(), earnedTuesday)));
                mListStats.add(new StatsByHolder("Wednesday", Utilities.MillisecondsToFormattedElapsedTime(timeWednesday *1000), Utilities.ConvertWageToLocale(getActivity(), earnedWednesday)));
                mListStats.add(new StatsByHolder("Thursday", Utilities.MillisecondsToFormattedElapsedTime(timeThursday *1000), Utilities.ConvertWageToLocale(getActivity(), earnedThursday)));
                mListStats.add(new StatsByHolder("Friday", Utilities.MillisecondsToFormattedElapsedTime(timeFriday *1000), Utilities.ConvertWageToLocale(getActivity(), earnedFriday)));
                mListStats.add(new StatsByHolder("Saturday", Utilities.MillisecondsToFormattedElapsedTime(timeSaturday *1000), Utilities.ConvertWageToLocale(getActivity(), earnedSaturday)));
                mListStats.add(new StatsByHolder("Sunday", Utilities.MillisecondsToFormattedElapsedTime(timeSunday *1000), Utilities.ConvertWageToLocale(getActivity(), earnedSunday)));

                mAdapter = new DailyStatsAdapter(getActivity(), R.layout.list_simple, mListStats);
                lvStats.setAdapter(mAdapter);

                dismissProgressBar();
            }
        });

        return mView;
    }

    public void showProgressBar(String msg){
        mDialog = ProgressDialog.show(getActivity(), "", msg, true);
    }

    public void dismissProgressBar(){
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }
}
