package com.poopsalary.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.poopsalary.R;
import com.poopsalary.adapters.DailyStatsAdapter;
import com.poopsalary.models.Logs;
import com.poopsalary.models.StatsByHolder;
import com.poopsalary.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

public class StatsHourlyFragment extends Fragment {

    private Dialog mDialog;
    View mView;
    ArrayList<StatsByHolder> mListStats;
    DailyStatsAdapter mAdapter;
    ListView lvStats;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.list_simple, container, false);
        lvStats = (ListView)mView.findViewById(android.R.id.list);

        showProgressBar("Compiling Hourly Stats...");

        ParseQuery<Logs> qYear = new ParseQuery<Logs>(Logs.class);
        qYear.fromLocalDatastore();
        qYear.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                long time12PM = 0;
                long time1PM = 0;
                long time2PM = 0;
                long time3PM = 0;
                long time4PM = 0;
                long time5PM = 0;
                long time6PM = 0;
                long time7PM = 0;
                long time8PM = 0;
                long time9PM = 0;
                long time10PM = 0;
                long time11PM = 0;
                long time12AM = 0;
                long time1AM = 0;
                long time2AM = 0;
                long time3AM = 0;
                long time4AM = 0;
                long time5AM = 0;
                long time6AM = 0;
                long time7AM = 0;
                long time8AM = 0;
                long time9AM = 0;
                long time10AM = 0;
                long time11AM = 0;
                double earned12PM = 0;
                double earned1PM = 0;
                double earned2PM = 0;
                double earned3PM = 0;
                double earned4PM = 0;
                double earned5PM = 0;
                double earned6PM = 0;
                double earned7PM = 0;
                double earned8PM = 0;
                double earned9PM = 0;
                double earned10PM = 0;
                double earned11PM = 0;
                double earned12AM = 0;
                double earned1AM = 0;
                double earned2AM = 0;
                double earned3AM = 0;
                double earned4AM = 0;
                double earned5AM = 0;
                double earned6AM = 0;
                double earned7AM = 0;
                double earned8AM = 0;
                double earned9AM = 0;
                double earned10AM = 0;
                double earned11AM = 0;

                String hourOfDay = "";

                //populate data for list
                for(int i = 0; i < logs.size(); i++)
                {
                    hourOfDay = Utilities.getHourOfDay(logs.get(i).getTimeCompleted().getTime());

                    switch (hourOfDay)
                    {
                        case "12PM":
                            time12PM += logs.get(i).getTotalSeconds();
                            earned12PM += logs.get(i).getMoneyEarned();
                            break;
                        case "1PM":
                            time1PM += logs.get(i).getTotalSeconds();
                            earned1PM += logs.get(i).getMoneyEarned();
                            break;
                        case "2PM":
                            time2PM += logs.get(i).getTotalSeconds();
                            earned2PM += logs.get(i).getMoneyEarned();
                            break;
                        case "3PM":
                            time3PM += logs.get(i).getTotalSeconds();
                            earned3PM += logs.get(i).getMoneyEarned();
                            break;
                        case "4PM":
                            time4PM += logs.get(i).getTotalSeconds();
                            earned4PM += logs.get(i).getMoneyEarned();
                            break;
                        case "5PM":
                            time5PM += logs.get(i).getTotalSeconds();
                            earned5PM += logs.get(i).getMoneyEarned();
                            break;
                        case "6PM":
                            time6PM += logs.get(i).getTotalSeconds();
                            earned6PM += logs.get(i).getMoneyEarned();
                            break;
                        case "7PM":
                            time7PM += logs.get(i).getTotalSeconds();
                            earned7PM += logs.get(i).getMoneyEarned();
                            break;
                        case "8PM":
                            time8PM += logs.get(i).getTotalSeconds();
                            earned8PM += logs.get(i).getMoneyEarned();
                            break;
                        case "9PM":
                            time9PM += logs.get(i).getTotalSeconds();
                            earned9PM += logs.get(i).getMoneyEarned();
                            break;
                        case "10PM":
                            time10PM += logs.get(i).getTotalSeconds();
                            earned10PM += logs.get(i).getMoneyEarned();
                            break;
                        case "11PM":
                            time11PM += logs.get(i).getTotalSeconds();
                            earned11PM += logs.get(i).getMoneyEarned();
                            break;
                        case "12AM":
                            time12AM += logs.get(i).getTotalSeconds();
                            earned12AM += logs.get(i).getMoneyEarned();
                            break;
                        case "1AM":
                            time1AM += logs.get(i).getTotalSeconds();
                            earned1AM += logs.get(i).getMoneyEarned();
                            break;
                        case "2AM":
                            time2AM += logs.get(i).getTotalSeconds();
                            earned2AM += logs.get(i).getMoneyEarned();
                            break;
                        case "3AM":
                            time3AM += logs.get(i).getTotalSeconds();
                            earned3AM += logs.get(i).getMoneyEarned();
                            break;
                        case "4AM":
                            time4AM += logs.get(i).getTotalSeconds();
                            earned4AM += logs.get(i).getMoneyEarned();
                            break;
                        case "5AM":
                            time5AM += logs.get(i).getTotalSeconds();
                            earned5AM += logs.get(i).getMoneyEarned();
                            break;
                        case "6AM":
                            time6AM += logs.get(i).getTotalSeconds();
                            earned6AM += logs.get(i).getMoneyEarned();
                            break;
                        case "7AM":
                            time7AM += logs.get(i).getTotalSeconds();
                            earned7AM += logs.get(i).getMoneyEarned();
                            break;
                        case "8AM":
                            time8AM += logs.get(i).getTotalSeconds();
                            earned8AM += logs.get(i).getMoneyEarned();
                            break;
                        case "9AM":
                            time9AM += logs.get(i).getTotalSeconds();
                            earned9AM += logs.get(i).getMoneyEarned();
                            break;
                        case "10AM":
                            time10AM += logs.get(i).getTotalSeconds();
                            earned10AM += logs.get(i).getMoneyEarned();
                            break;
                        case "11AM":
                            time11AM += logs.get(i).getTotalSeconds();
                            earned11AM += logs.get(i).getMoneyEarned();
                            break;
                    }
                }

                mListStats = new ArrayList<StatsByHolder>();
                mListStats.add(new StatsByHolder("HOUR", "TIME", "EARNED"));
                mListStats.add(new StatsByHolder("12PM", Utilities.MillisecondsToFormattedElapsedTime(time12PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned12PM)));
                mListStats.add(new StatsByHolder("1PM", Utilities.MillisecondsToFormattedElapsedTime(time1PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned1PM)));
                mListStats.add(new StatsByHolder("2PM", Utilities.MillisecondsToFormattedElapsedTime(time2PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned2PM)));
                mListStats.add(new StatsByHolder("3PM", Utilities.MillisecondsToFormattedElapsedTime(time3PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned3PM)));
                mListStats.add(new StatsByHolder("4PM", Utilities.MillisecondsToFormattedElapsedTime(time4PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned4PM)));
                mListStats.add(new StatsByHolder("5PM", Utilities.MillisecondsToFormattedElapsedTime(time5PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned5PM)));
                mListStats.add(new StatsByHolder("6PM", Utilities.MillisecondsToFormattedElapsedTime(time6PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned6PM)));
                mListStats.add(new StatsByHolder("7PM", Utilities.MillisecondsToFormattedElapsedTime(time7PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned7PM)));
                mListStats.add(new StatsByHolder("8PM", Utilities.MillisecondsToFormattedElapsedTime(time8PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned8PM)));
                mListStats.add(new StatsByHolder("9PM", Utilities.MillisecondsToFormattedElapsedTime(time9PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned9PM)));
                mListStats.add(new StatsByHolder("10PM", Utilities.MillisecondsToFormattedElapsedTime(time10PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned10PM)));
                mListStats.add(new StatsByHolder("11PM", Utilities.MillisecondsToFormattedElapsedTime(time11PM *1000), Utilities.ConvertWageToLocale(getActivity(), earned11PM)));
                mListStats.add(new StatsByHolder("12AM", Utilities.MillisecondsToFormattedElapsedTime(time12AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned12AM)));
                mListStats.add(new StatsByHolder("1AM", Utilities.MillisecondsToFormattedElapsedTime(time1AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned1AM)));
                mListStats.add(new StatsByHolder("2AM", Utilities.MillisecondsToFormattedElapsedTime(time2AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned2AM)));
                mListStats.add(new StatsByHolder("3AM", Utilities.MillisecondsToFormattedElapsedTime(time3AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned3AM)));
                mListStats.add(new StatsByHolder("4AM", Utilities.MillisecondsToFormattedElapsedTime(time4AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned4AM)));
                mListStats.add(new StatsByHolder("5AM", Utilities.MillisecondsToFormattedElapsedTime(time5AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned5AM)));
                mListStats.add(new StatsByHolder("6AM", Utilities.MillisecondsToFormattedElapsedTime(time6AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned6AM)));
                mListStats.add(new StatsByHolder("7AM", Utilities.MillisecondsToFormattedElapsedTime(time7AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned7AM)));
                mListStats.add(new StatsByHolder("8AM", Utilities.MillisecondsToFormattedElapsedTime(time8AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned8AM)));
                mListStats.add(new StatsByHolder("9AM", Utilities.MillisecondsToFormattedElapsedTime(time9AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned9AM)));
                mListStats.add(new StatsByHolder("10AM", Utilities.MillisecondsToFormattedElapsedTime(time10AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned10AM)));
                mListStats.add(new StatsByHolder("11AM", Utilities.MillisecondsToFormattedElapsedTime(time11AM *1000), Utilities.ConvertWageToLocale(getActivity(), earned11AM)));

                mAdapter = new DailyStatsAdapter(getActivity(), R.layout.list_simple, mListStats);
                lvStats.setAdapter(mAdapter);

                dismissProgressBar();
            }
        });

        return mView;
    }

    public void showProgressBar(String msg){
        mDialog = ProgressDialog.show(getActivity(), "", msg, true);
    }

    public void dismissProgressBar(){
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }
}
