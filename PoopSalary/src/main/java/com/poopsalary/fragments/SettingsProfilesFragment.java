package com.poopsalary.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;
import com.poopsalary.R;
import com.poopsalary.activities.SyncOldDataActivity;
import com.poopsalary.models.ExchangeRates;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import com.poopsalary.old.StatsActivity;
import com.poopsalary.utils.AppPreferences;
import com.poopsalary.utils.Utilities;

import java.util.Date;
import java.util.List;

public class SettingsProfilesFragment extends Fragment implements View.OnClickListener {

    public interface Listener {
        public void onSignInButtonClicked();
        public void onSignOutButtonClicked();
        public void onSignOutParseButtonClicked();
        public void onSignInProcessOver();
    }

    Listener mListener = null;

    View v;

    boolean mShowGoogleSignin = true;
    boolean mShowParseSignin = true;

    private AdView adView;

    Dialog mDialog;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_my_profile, container, false);

        TextView txtLastUpdate = (TextView)v.findViewById(R.id.txtMemberLastUpdated);
        TextView txtSince = (TextView)v.findViewById(R.id.txtMemberSince);
        TextView tv = (TextView) v.findViewById(R.id.hello);
        TextView txtSyncOldData = (TextView) v.findViewById(R.id.txtSyncOldData);
        if(ParseUser.getCurrentUser() != null)
            txtSyncOldData.setText(ParseUser.getCurrentUser().get("hasSyncedOriginalData").equals(true) ? "You've already uploaded your old data. Doing so again may create duplicates" : "Sync your old data to your new Poop Salary account.");

        if(ParseUser.getCurrentUser() != null)
        {
            mShowParseSignin = false;
            tv.setText("Welcome, " + ParseUser.getCurrentUser().getUsername());
            txtSince.setText(Utilities.getFullFormattedDate(ParseUser.getCurrentUser().getCreatedAt().getTime()));
            txtLastUpdate.setText(Utilities.getFullFormattedDate(ParseUser.getCurrentUser().getUpdatedAt().getTime()));

        }
        else
        {   mShowParseSignin = true;
            tv.setText("Your Future Profile");
            txtSince.setText("Today?");
            txtLastUpdate.setText("Today?");
            ParseLoginBuilder builder = new ParseLoginBuilder(getActivity());
            builder.setAppLogo(R.drawable.ic_launcher);
            startActivityForResult(builder.build(), 0);
        }

        final int[] CLICKABLES = new int[] {
                R.id.btnRecoverData, R.id.sign_out_button_parse, R.id.sign_in_button_parse
        };

        for (int i : CLICKABLES) {
            v.findViewById(i).setOnClickListener(this);
        }

        // Create an ad.
        adView = new AdView(getActivity());
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-1380578583463384/3183147619");

        // Add the AdView to the view hierarchy. The view will have no size
        // until the ad is loaded.
        LinearLayout layout = (LinearLayout) v.findViewById(R.id.adbanner);
        layout.addView(adView);

        // Create an ad request. Check logcat output for the hashed device ID to
        // get test ads on a physical device.
        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adView.loadAd(adRequest);

        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRecoverData:
                Intent intent = new Intent(getActivity(), SyncOldDataActivity.class);
                startActivityForResult(intent, 9999);
                break;
            case R.id.sign_out_button_parse:
                mListener.onSignOutParseButtonClicked();
                break;
            case R.id.sign_in_button_parse:
                ParseLoginBuilder builder = new ParseLoginBuilder(getActivity());
                builder.setAppLogo(R.drawable.ic_launcher);
                startActivityForResult(builder.build(), 0);
                break;
        }
    }

    public void setListener(Listener l) {
        mListener = l;
    }

    public void setShowSignInButton(boolean showSignIn) {
        mShowGoogleSignin = showSignIn;
        updateUi();
    }

    public void setShowSigninParseButton(boolean parseSignedIn){
        mShowParseSignin = parseSignedIn;
        updateUi();

        TextView txtLastUpdate = (TextView)v.findViewById(R.id.txtMemberLastUpdated);
        TextView txtSince = (TextView)v.findViewById(R.id.txtMemberSince);
        TextView tv = (TextView) v.findViewById(R.id.hello);

        if(ParseUser.getCurrentUser() != null)
        {
            tv.setText("Welcome, " + ParseUser.getCurrentUser().getUsername());
            txtSince.setText(Utilities.getFullFormattedDate(ParseUser.getCurrentUser().getCreatedAt().getTime()));
            txtLastUpdate.setText(Utilities.getFullFormattedDate(ParseUser.getCurrentUser().getUpdatedAt().getTime()));
        }
        else
        {
            tv.setText("Your Future Profile");
            txtSince.setText("Today?");
            txtLastUpdate.setText("Today?");
        }

    }

    void updateUi() {
        if (getActivity() == null) return;

        getActivity().findViewById(R.id.sign_in_bar).setVisibility(!mShowParseSignin ? View.VISIBLE : View.GONE);
        getActivity().findViewById(R.id.sign_in_bar_parse).setVisibility(mShowParseSignin ? View.VISIBLE : View.GONE);
        getActivity().findViewById(R.id.sign_out_bar_parse).setVisibility(mShowParseSignin ? View.GONE : View.VISIBLE);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        menu.add("Sync").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        convertOrSync();

        return false;
    }

    private void convertOrSync(){
        if(ParseUser.getCurrentUser() != null){
            if(ParseUser.getCurrentUser().get("hasSyncedOriginalData").equals(false)){
                Intent intent = new Intent(getActivity(), SyncOldDataActivity.class);
                startActivityForResult(intent, 9999);

            }else{
                syncData();
            }
        }
    }

    private void setHasSyncedData(){
        ParseUser.getCurrentUser().put("hasSyncedOriginalData", true);
        ParseUser.getCurrentUser().saveEventually();

    }

    private void syncData()
    {
        Toast.makeText(getActivity(), "Retrieving all your users data to use locally", Toast.LENGTH_LONG).show();
        ParseObject.unpinAllInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {

                ParseQuery<Jobs> jobs = new ParseQuery<Jobs>(Jobs.class);
                jobs.whereEqualTo("user", ParseUser.getCurrentUser());

                //Loading Jobs
                showProgressBar("Loading Jobs...");
                jobs.findInBackground(new FindCallback<Jobs>() {
                    @Override
                    public void done(List<Jobs> jobs, ParseException e) {
                        ParseObject.pinAllInBackground(jobs);
                        dismissProgressBar();
                        TextView txtLoadingJobs = (TextView)v.findViewById(R.id.txtLoadingJobs);

                        ParseQuery<Logs> logs = new ParseQuery<Logs>(Logs.class);
                        logs.whereEqualTo("user", ParseUser.getCurrentUser());

                        //Loading Logs
                        showProgressBar("Loading Logs...");
                        logs.findInBackground(new FindCallback<Logs>() {
                            @Override
                            public void done(List<Logs> logs, ParseException e) {
                                ParseObject.pinAllInBackground(logs);
                                dismissProgressBar();
                                TextView txtLoadingLogs = (TextView)v.findViewById(R.id.txtLoadingLogs);

                                showProgressBar("Downloading Currency Rates...");
                                ParseQuery<ExchangeRates> query = new ParseQuery<ExchangeRates>(ExchangeRates.class);
                                query.setLimit(200);
                                query.findInBackground(new FindCallback<ExchangeRates>() {
                                    @Override
                                    public void done(List<ExchangeRates> exchangeRates, ParseException e) {
                                        ParseObject.pinAllInBackground(exchangeRates);
                                        dismissProgressBar();
                                        AppPreferences.setLastRateCheck(getActivity(), new Date().getTime());
                                        setHasSyncedData();
                                        AppPreferences.setIsFirstRun(getActivity(), true);
                                        removeDuplicates();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    private void removeDuplicates(){

        ParseQuery<Jobs> jobsParseQuery = new ParseQuery<Jobs>(Jobs.class);
        jobsParseQuery.fromLocalDatastore();
        jobsParseQuery.orderByDescending("jobName");
        showProgressBar("Removing Job Duplicates...");
        jobsParseQuery.findInBackground(new FindCallback<Jobs>() {
            @Override
            public void done(List<Jobs> jobs, ParseException e) {

                Jobs currentJob;

                if(e == null)
                {
                    if(jobs.size() > 0)
                    {
                        currentJob = jobs.get(0);

                        for(int i = 1; i < jobs.size(); i++){
                            if(jobs.get(i).getJobName().equals(currentJob.getJobName()))
                            {
                                //job may be duplicate
                                if(jobs.get(i).getWage() == currentJob.getWage()){
                                    //job is dupblicate. remove it
                                    jobs.get(i).deleteEventually();
                                }
                                else
                                {
                                    //job is not duplicate. carry on
                                    currentJob = jobs.get(i);
                                }
                            }
                            else
                            {
                                //job is not duplicate. carry on
                                currentJob = jobs.get(i);

                            }
                        }
                    }

                }
                dismissProgressBar();

                showProgressBar("Removing Log Duplicates...");
                ParseQuery<Logs> logsParseQuery = new ParseQuery<Logs>(Logs.class);
                logsParseQuery.fromLocalDatastore();
                logsParseQuery.orderByDescending("timeStarted");
                logsParseQuery.findInBackground(new FindCallback<Logs>() {
                    @Override
                    public void done(List<Logs> logs, ParseException e) {

                        Logs currentLog;

                        if(e == null)
                        {
                            if(logs.size() > 0)
                            {
                                currentLog = logs.get(0);

                                for(int i = 1; i < logs.size(); i++)
                                {
                                    if(Utilities.getDateDayMonthYearTime(currentLog.getTimeStarted()).equals(Utilities.getDateDayMonthYearTime(logs.get(i).getTimeStarted())))
                                    {
                                        //log was started at exact same time. this is a duplicate
                                        logs.get(i).deleteEventually();
                                    }
                                    else
                                    {
                                        //log is not a duplicate. carry on
                                        currentLog = logs.get(i);
                                    }
                                }
                            }
                        }

                        dismissProgressBar();

                        //remove log duplicates
                        getActivity().finish();
                        getActivity().startActivity(getActivity().getIntent());
                    }
                });


            }
        });


    }

    public void showProgressBar(String msg){
        mDialog = ProgressDialog.show(getActivity(), "", msg, true);
    }

    public void dismissProgressBar(){
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if(ParseUser.getCurrentUser() != null){
                ParseUser.getCurrentUser().fetchInBackground();
                setShowSigninParseButton(false);
                convertOrSync();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

}
