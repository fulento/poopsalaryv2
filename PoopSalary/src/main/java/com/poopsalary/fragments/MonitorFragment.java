package com.poopsalary.fragments;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.poopsalary.activities.DepositReceiptActivity;
import com.poopsalary.activities.JobEditorActivity;
import com.poopsalary.controllers.UserController;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import com.poopsalary.models.StopWatchModel;
import com.poopsalary.R;
import com.poopsalary.services.AlarmReceiver;
import com.poopsalary.utils.AppPreferences;
import com.poopsalary.utils.Utilities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Chronometer;
import android.widget.Toast;
import android.text.format.DateFormat;

import java.util.Date;
import android.app.PendingIntent;
import android.content.Intent;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.AlarmManager;

import com.google.android.gms.ads.InterstitialAd;

//Backward Compatibility Libraries
import android.support.v4.app.Fragment;

public class MonitorFragment extends Fragment implements OnClickListener {

    public interface Listener {
        public void onDepositRecorded(int seconds, double earned);
        public void onPoopRated();
    }


    private AdView adView;
    private InterstitialAd mInterstitialAd;
    Listener mListener = null;

    TextView notifierText;

    int mProgressColorUpdated = 0;

    boolean mHasBrokenHighScore = false;

    long mLongestPoop = 0;
    long mAveragePoop = 0;
    long mShortestPoop = 0;
    long mCurrentElapsed = 0;

    StopWatchModel mStopWatch;

    TextView txtShort;
    TextView txtAverage;
    TextView txtLong;
    TextView txtShortHeader;
    TextView txtAverageHeader;
    TextView txtLongHeader;
    TextView txtSelectedJob;

    Menu menuJobs;

    UserController mUser;
    Chronometer mChrono;

    Boolean HasSelectedJob;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_monitor, container, false);
        final int[] CLICKABLES = new int[] {
                R.id.btnStart, R.id.btnStop
        };
        for (int i : CLICKABLES) {
            v.findViewById(i).setOnClickListener(this);
        }

        mStopWatch = new StopWatchModel();
        txtShort = (TextView) v.findViewById(R.id.txtShortestPoop);
        txtAverage = (TextView) v.findViewById(R.id.txtAveragePoop);
        txtLong = (TextView) v.findViewById(R.id.txtLongestPoop);
        txtShortHeader = (TextView) v.findViewById(R.id.txtShortHeader);
        txtAverageHeader = (TextView) v.findViewById(R.id.txtAverageHeader);
        txtLongHeader = (TextView) v.findViewById(R.id.txtLongHeader);
        txtSelectedJob = (TextView) v.findViewById(R.id.txtMonitorSelectedJob);

        ParseQuery<Jobs> query = new ParseQuery<Jobs>(Jobs.class);
        query.fromLocalDatastore();
        query.whereEqualTo("isSelected", true);
        query.getFirstInBackground(new GetCallback<Jobs>() {
            @Override
            public void done(Jobs job, ParseException e) {
                if(e == null)
                {
                    txtSelectedJob.setText(job.getJobName());
                    HasSelectedJob = true;
                    mUser = new UserController(getActivity(),new StopWatchModel(), job, Locale.getDefault());
                }
                else
                {
                    HasSelectedJob = false;
                }

            }
        });

        notifierText = (TextView) v.findViewById(R.id.textnotifier);

        mChrono = (Chronometer) v.findViewById(R.id.chronometer1);
        mChrono.setFormat("00:00");

        ParseQuery<Logs> queryStats = new ParseQuery<Logs>(Logs.class);
        queryStats.fromLocalDatastore();
        queryStats.orderByDescending("totalSeconds");
        queryStats.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {

                if(logs.size()>0)
                {
                    mLongestPoop = logs.get(0).getTotalSeconds() * 1000;
                    mShortestPoop = logs.get(logs.size()-1).getTotalSeconds() * 1000;
                    mAveragePoop = Utilities.getAveragePoopTime(logs);
                }
                else
                {
                    mLongestPoop = 0;
                    mAveragePoop = 0;
                    mShortestPoop = 0;
                }

                mStopWatch.Set(mShortestPoop);
                txtShort.setText(mStopWatch.getFormattedTime());

                mStopWatch.Set(mAveragePoop);
                txtAverage.setText(mStopWatch.getFormattedTime());

                mStopWatch.Set(mLongestPoop);
                txtLong.setText(mStopWatch.getFormattedTime());


                mChrono.setOnChronometerTickListener(
                        new Chronometer.OnChronometerTickListener() {
                            @Override
                            public void onChronometerTick(Chronometer chronometer) {

                                mUser.getStopWatch().Set(System.currentTimeMillis() - mChrono.getBase());
                                notifierText.setText(getMoney(System.currentTimeMillis() - mChrono.getBase()));
                                mChrono.setText(mUser.getStopWatch().getFormattedTime());

                                //limit poops to 59:59
                                if (mUser.getStopWatch().getTotalMills() < 3540000) {
                                    if (mUser.getStopWatch().getTotalMills() <= mLongestPoop) {

                                        if (mUser.getStopWatch().getTotalMills() > mShortestPoop) {

                                            //progress color is still default
                                            if (mProgressColorUpdated == 0) {
                                                txtShort.setTextColor(getResources().getColor(R.color.green));
                                                txtShortHeader.setTextColor(getResources().getColor(R.color.green));
                                                mProgressColorUpdated = 1;
                                                Toast.makeText(getActivity(), "At least you're not a quitter...", Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        if (mUser.getStopWatch().getTotalMills() > mAveragePoop) {
                                            //progress color is average color still
                                            if (mProgressColorUpdated == 1) {
                                                txtAverage.setTextColor(getResources().getColor(R.color.green));
                                                txtAverageHeader.setTextColor(getResources().getColor(R.color.green));
                                                mProgressColorUpdated = 2;
                                                Toast.makeText(getActivity(), "Pretty average poop so far...", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    } else {
                                        if (!mHasBrokenHighScore) {
                                            Toast.makeText(getActivity(), "You've broken your high score!!", Toast.LENGTH_LONG).show();
                                            txtLong.setTextColor(getResources().getColor(R.color.green));
                                            txtLongHeader.setTextColor(getResources().getColor(R.color.green));
                                            mHasBrokenHighScore = true;
                                        }
                                    }

                                    mCurrentElapsed = mUser.getStopWatch().getTotalMills();

                                } else {
                                    SavePoopResetTimer();
                                }
                            }
                        }
                );

            }
        });

        if(AppPreferences.getPoopStartTime(getActivity()) > 0)
        {
            long startTime = AppPreferences.getPoopStartTime(getActivity());
            mChrono.setBase(startTime);
            mChrono.start();
        }

        // Create an ad.
        adView = new AdView(getActivity());
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-1380578583463384/4311580818");

        // Add the AdView to the view hierarchy. The view will have no size
        // until the ad is loaded.
        LinearLayout layout = (LinearLayout) v.findViewById(R.id.adbanner);
        layout.addView(adView);

        // Create an ad request. Check logcat output for the hashed device ID to
        // get test ads on a physical device.
        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adView.loadAd(adRequest);

        initInterstitial();
        AdRequest iAdRequest = new AdRequest.Builder().build();

        mInterstitialAd.loadAd(iAdRequest);


        return v;
    }

    public String getMoney(long milliseconds)
    {
        return mUser.getFormattedMoneyEarned();
    }

    public void setListener(Listener l) {
        mListener = l;
    }

    void updateUi() {
        if (getActivity() == null) return;

        if(AppPreferences.getPoopStartTime(getActivity()) > 0){
            getActivity().findViewById(R.id.btnStart).setVisibility(View.GONE);
            getActivity().findViewById(R.id.btnStop).setVisibility(View.VISIBLE);
        } else{
            getActivity().findViewById(R.id.btnStart).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.btnStop).setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        case R.id.btnStart:
            if(HasSelectedJob)
            {
                long startTime = System.currentTimeMillis();
                AppPreferences.setPoopStartTime(getActivity(), startTime);

                mChrono.setBase(AppPreferences.getPoopStartTime(getActivity()));
                mChrono.start();

                StartAlarmToaster();
                updateUi();
            }
            else
            {
                Toast.makeText(getActivity(), "Please select a job to use and make sure it's wage is set", Toast.LENGTH_LONG).show();
            }


            break;
        case R.id.btnStop:
            updateUi();
            SavePoopResetTimer();
            break;
        }
    }

    private void StartAlarmToaster()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 50);

        Intent intent = new Intent(getActivity(), AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), 42107579, intent, 0);

        AlarmManager alarm = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 60*1000, alarmIntent);

        Toast.makeText(getActivity(), "You've started pooping...", Toast.LENGTH_LONG).show();

    }

    private void StopAlarmToaster()
    {
        Intent intent = new Intent(getActivity(), AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), 42107579, intent, 0);
        AlarmManager alarm = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);

        alarm.cancel(alarmIntent);
    }

    private void SavePoopResetTimer()
    {
        long elapsedTime = System.currentTimeMillis() - mChrono.getBase();
        mChrono.stop();
        mChrono.setText(DateFormat.format("mm:ss", 0));
        Calendar cal = Calendar.getInstance();

        Logs log = new Logs();
        log.setTimeStarted(Utilities.getFullDate(System.currentTimeMillis() - elapsedTime));
        log.setTimeCompleted(new Date());
        log.setHasBeenEdited(false);
        log.setTotalSeconds(elapsedTime / 1000);

        log.setRating(0);
        log.setACL();
        log.setUser(ParseUser.getCurrentUser());
        log.setJob(mUser.getCurrentJob());
        log.setIsNew(true);
        log.setMoneyEarned(Utilities.CalculateWageDouble(mUser.getCurrentJob().getWageType(), mUser.getCurrentJob().getWage(), elapsedTime / 1000));
        log.pinInBackground();
        log.saveEventually();

        Toast.makeText(getActivity(), "You made " + mUser.getFormattedMoneyEarned() + " in " + mUser.getFormattedElapsedTime(), Toast.LENGTH_LONG).show();

        mUser.getStopWatch().Reset();

        txtShort.setTextColor(getResources().getColor(R.color.gray_1));
        txtShortHeader.setTextColor(getResources().getColor(R.color.gray_1));
        txtAverage.setTextColor(getResources().getColor(R.color.gray_1));
        txtAverageHeader.setTextColor(getResources().getColor(R.color.gray_1));
        txtLong.setTextColor(getResources().getColor(R.color.gray_1));
        txtLongHeader.setTextColor(getResources().getColor(R.color.gray_1));
        notifierText.setText(getMoney(0));
        mHasBrokenHighScore = false;
        mProgressColorUpdated = 0;

        ParseQuery<Logs> queryStats = new ParseQuery<Logs>(Logs.class);
        queryStats.fromLocalDatastore();
        queryStats.orderByDescending("totalSeconds");
        queryStats.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                if(logs.size()>0)
                {
                    mLongestPoop = logs.get(0).getTotalSeconds() * 1000;
                    mShortestPoop = logs.get(logs.size()-1).getTotalSeconds() * 1000;
                    mAveragePoop = Utilities.getAveragePoopTime(logs);
                }
                else
                {
                    mLongestPoop = 0;
                    mAveragePoop = 0;
                    mShortestPoop = 0;
                }
            }
        });
        AppPreferences.setPoopStartTime(getActivity(), 0);
        StopAlarmToaster();

        Intent intent = new Intent(getActivity(), DepositReceiptActivity.class);
        startActivityForResult(intent, 9999);
    }

    private void initInterstitial() {
        // Create the InterstitialAd and set the adUnitId.
        mInterstitialAd = new InterstitialAd(getActivity());
        // Defined in values/strings.xml
        mInterstitialAd.setAdUnitId("ca-app-pub-1380578583463384/1078912811");
    }

    private void displayInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == 9999) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                boolean shouldIncrementRating = data.getBooleanExtra("ratingUpdated", false);
                boolean isNew = data.getBooleanExtra("poopIsNew", false);

                if(shouldIncrementRating)
                {
                    mListener.onPoopRated();
                }

                if(isNew){

                    long seconds = data.getLongExtra("secondsPooped", 0);
                    double earned = data.getDoubleExtra("moneyEarned", 0);
                    mListener.onDepositRecorded((int)seconds, earned);
                }

            }

            displayInterstitial();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menuJobs = menu;
        super.onCreateOptionsMenu(menuJobs, inflater);

        ParseQuery<Jobs> query = new ParseQuery<Jobs>(Jobs.class);
        query.fromLocalDatastore();
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<Jobs>() {
            @Override
            public void done(List<Jobs> jobs, ParseException e) {
                if(e == null)
                {

                    for(int i = 0; i < jobs.size(); i++)
                    {
                        menuJobs.add(jobs.get(i).getJobName()).setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
                    }
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final MenuItem item1 = item;

        ParseQuery<Jobs> query = new ParseQuery<Jobs>(Jobs.class);
        query.fromLocalDatastore();
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<Jobs>() {
            @Override
            public void done(List<Jobs> jobs, ParseException e) {
                if(e == null)
                {
                    for(int i = 0; i < jobs.size(); i++)
                    {
                        if(jobs.get(i).getJobName().equals(item1.getTitle()))
                        {
                            jobs.get(i).setIsSelected(true);
                            txtSelectedJob.setText(jobs.get(i).getJobName());
                            HasSelectedJob = true;
                            mUser = new UserController(getActivity(),new StopWatchModel(), jobs.get(i), Locale.getDefault());
                            jobs.get(i).saveEventually();
                            Toast.makeText(getActivity(), "Switched jobs to " + jobs.get(i).getJobName(), Toast.LENGTH_SHORT);
                        }
                        else
                        {
                            jobs.get(i).setIsSelected(false);
                            jobs.get(i).saveEventually();
                        }
                    }
                }
            }
        });

        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();

    }


    @Override
    public void onDestroy()
    {
        // Destroy the AdView.
        if (adView != null) {
            adView.destroy();
        }

        super.onDestroy();

    }
}
