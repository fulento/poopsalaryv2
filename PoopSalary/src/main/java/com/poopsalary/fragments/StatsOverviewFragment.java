package com.poopsalary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.poopsalary.R;
import com.poopsalary.models.Logs;
import com.poopsalary.utils.Utilities;

import java.util.List;

public class StatsOverviewFragment extends Fragment {

    View v;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.framgment_stats_overview, container, false);

        ParseQuery<Logs> qWeek = new ParseQuery<Logs>(Logs.class);
        qWeek.fromLocalDatastore();
        qWeek.whereGreaterThanOrEqualTo("timeCompleted", Utilities.getDateLast7Days());
        qWeek.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                long totalTimeLastSeven = 0;
                double totalEarnedLastSeven = 0;

                for(int i = 0; i < logs.size(); i++)
                {
                    totalTimeLastSeven += logs.get(i).getTotalSeconds();
                    totalEarnedLastSeven += logs.get(i).getMoneyEarned();
                }

                TextView txtTimeWeek = (TextView)v.findViewById(R.id.txtTimeLastSeven);
                TextView txtEarnedWeek = (TextView)v.findViewById(R.id.txtEarnedLastSeven);

                txtTimeWeek.setText(Utilities.MillisecondsToFormattedElapsedTime(totalTimeLastSeven *1000));
                txtEarnedWeek.setText(Utilities.ConvertWageToLocale(getActivity(), totalEarnedLastSeven));
            }
        });

        ParseQuery<Logs> qMonth = new ParseQuery<Logs>(Logs.class);
        qMonth.fromLocalDatastore();
        qMonth.whereGreaterThanOrEqualTo("timeCompleted", Utilities.getDateLast30Days());
        qMonth.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                long totalTimeLastMonth = 0;
                double totalEarnedLastMonth = 0;

                for(int i = 0; i < logs.size(); i++)
                {
                    totalTimeLastMonth += logs.get(i).getTotalSeconds();
                    totalEarnedLastMonth += logs.get(i).getMoneyEarned();
                }

                TextView txtTimeMonth = (TextView)v.findViewById(R.id.txtTimeLastMonth);
                TextView txtEarnedMonth = (TextView)v.findViewById(R.id.txtEarnedLastMonth);

                txtTimeMonth.setText(Utilities.MillisecondsToFormattedElapsedTime(totalTimeLastMonth *1000));
                txtEarnedMonth.setText(Utilities.ConvertWageToLocale(getActivity(), totalEarnedLastMonth));
            }
        });

        ParseQuery<Logs> qYear = new ParseQuery<Logs>(Logs.class);
        qYear.fromLocalDatastore();
        qYear.whereGreaterThanOrEqualTo("timeCompleted", Utilities.getDateLastYear());
        qYear.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                long totalTimeLastYear = 0;
                double totalEarnedLastYear = 0;

                for(int i = 0; i < logs.size(); i++)
                {
                    totalTimeLastYear += logs.get(i).getTotalSeconds();
                    totalEarnedLastYear += logs.get(i).getMoneyEarned();
                }

                TextView txtTimeYear = (TextView)v.findViewById(R.id.txtTimeLastYear);
                TextView txtEarnedYear = (TextView)v.findViewById(R.id.txtEarnedLastYear);

                txtTimeYear.setText(Utilities.MillisecondsToFormattedElapsedTime(totalTimeLastYear *1000));
                txtEarnedYear.setText(Utilities.ConvertWageToLocale(getActivity(), totalEarnedLastYear));
            }
        });

        return v;
    }
}
