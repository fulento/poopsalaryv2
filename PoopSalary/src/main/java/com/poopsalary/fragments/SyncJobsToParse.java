package com.poopsalary.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.poopsalary.R;
import com.poopsalary.old.DatabaseAdapter;

public class SyncJobsToParse extends Fragment{

    private DatabaseAdapter dbHelper;
    TextView mJobCount;
    Cursor mJobsCursor;
    SyncLogsToParse mSyncLogsFragment;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mSyncLogsFragment = new SyncLogsToParse();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sync_jobs_to_parse, container, false);

        mJobCount = (TextView)v.findViewById(R.id.txtJobsCount);
        Button btnSync = (Button)v.findViewById(R.id.btnSync);

        dbHelper = new DatabaseAdapter(getActivity());
        dbHelper.open();

        mJobsCursor = dbHelper.fetchStatsByDate();

        mJobCount.setText(Integer.toString(mJobsCursor.getCount()));

        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
                        mSyncLogsFragment).commit();

            }
        });

        return v;
    }

    @Override
    public void onDestroy(){
        dbHelper.close();
    }
}
