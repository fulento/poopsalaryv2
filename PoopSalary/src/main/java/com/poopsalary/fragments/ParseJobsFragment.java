package com.poopsalary.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.poopsalary.R;
import com.poopsalary.activities.JobEditorActivity;
import com.poopsalary.adapters.JobsParseQueryAdapter;
import com.poopsalary.models.Jobs;
import com.poopsalary.utils.AppPreferences;

import java.util.List;

public class ParseJobsFragment extends ListFragment {

    Listener mListener = null;
    private JobsParseQueryAdapter mAdapter;
    Context mContext;

    public interface Listener {

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        this.setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AppPreferences.setIsFirstRun(getActivity(), false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        mContext = getActivity();
        View rootView = inflater.inflate(R.layout.list_simple, container, false);

        mAdapter = new JobsParseQueryAdapter(mContext);
        mAdapter.loadObjects();

        setListAdapter(mAdapter);
        return rootView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Jobs job = mAdapter.getItem(position);

        Intent intent = new Intent(getActivity(), JobEditorActivity.class);
        intent.putExtra("jobId", job.getObjectId());
        startActivityForResult(intent, 9999);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == 9999) {

            mAdapter.loadObjects();
            setListAdapter(mAdapter);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        menu.add("New").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(getActivity(), JobEditorActivity.class);
        startActivityForResult(intent, 9999);

        return false;
    }

    public void setListener(Listener l) {
        mListener = l;
    }

}
