package com.poopsalary.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.poopsalary.R;
import com.poopsalary.old.DatabaseAdapter;

public class SyncLogsToParse extends Fragment{

    private DatabaseAdapter dbHelper;
    TextView mJobCount;
    Cursor mJobsCursor;
    MonitorFragment mMonitorFragment;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mMonitorFragment = new MonitorFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sync_logs_to_parse, container, false);

        mJobCount = (TextView)v.findViewById(R.id.txtLogsCount);
        Button btnSync = (Button)v.findViewById(R.id.btnSync);

        dbHelper = new DatabaseAdapter(getActivity());
        dbHelper.open();

        mJobsCursor = dbHelper.fetchStatsByDate();

        mJobCount.setText(Integer.toString(mJobsCursor.getCount()));

        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
                        mMonitorFragment).commit();

            }
        });

        return v;
    }
}
