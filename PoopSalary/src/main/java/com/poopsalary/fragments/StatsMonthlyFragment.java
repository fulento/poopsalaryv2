package com.poopsalary.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.poopsalary.R;
import com.poopsalary.adapters.DailyStatsAdapter;
import com.poopsalary.models.Logs;
import com.poopsalary.models.StatsByHolder;
import com.poopsalary.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

public class StatsMonthlyFragment extends Fragment {

    private Dialog mDialog;
    View mView;
    ArrayList<StatsByHolder> mListStats;
    DailyStatsAdapter mAdapter;
    ListView lvStats;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.list_simple, container, false);
        lvStats = (ListView)mView.findViewById(android.R.id.list);

        showProgressBar("Compiling Monthly Stats...");

        ParseQuery<Logs> qYear = new ParseQuery<Logs>(Logs.class);
        qYear.fromLocalDatastore();
        qYear.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                long timeJanuary = 0;
                long timeFebruary = 0;
                long timeMarch = 0;
                long timeApril = 0;
                long timeMay = 0;
                long timeJune = 0;
                long timeJuly = 0;
                long timeAugust = 0;
                long timeSeptember = 0;
                long timeOctober = 0;
                long timeNovember = 0;
                long timeDecember = 0;

                double earnedJanuary = 0;
                double earnedFebruary = 0;
                double earnedMarch = 0;
                double earnedApril = 0;
                double earnedMay = 0;
                double earnedJune = 0;
                double earnedJuly = 0;
                double earnedAugust = 0;
                double earnedSeptember = 0;
                double earnedOctober = 0;
                double earnedNovember = 0;
                double earnedDecember = 0;

                String monthOfYear = "";

                //populate data for list
                for(int i = 0; i < logs.size(); i++)
                {
                    monthOfYear = Utilities.getMonthOfYear(logs.get(i).getTimeCompleted().getTime());

                    switch (monthOfYear)
                    {
                        case "January":
                            timeJanuary += logs.get(i).getTotalSeconds();
                            earnedJanuary += logs.get(i).getMoneyEarned();
                            break;
                        case "February":
                            timeFebruary += logs.get(i).getTotalSeconds();
                            earnedFebruary += logs.get(i).getMoneyEarned();
                            break;
                        case "March":
                            timeMarch += logs.get(i).getTotalSeconds();
                            earnedMarch += logs.get(i).getMoneyEarned();
                            break;
                        case "April":
                            timeApril += logs.get(i).getTotalSeconds();
                            earnedApril += logs.get(i).getMoneyEarned();
                            break;
                        case "May":
                            timeMay += logs.get(i).getTotalSeconds();
                            earnedMay += logs.get(i).getMoneyEarned();
                            break;
                        case "June":
                            timeJune += logs.get(i).getTotalSeconds();
                            earnedJune += logs.get(i).getMoneyEarned();
                            break;
                        case "July":
                            timeJuly += logs.get(i).getTotalSeconds();
                            earnedJuly += logs.get(i).getMoneyEarned();
                            break;
                        case "August":
                            timeAugust += logs.get(i).getTotalSeconds();
                            earnedAugust += logs.get(i).getMoneyEarned();
                            break;
                        case "September":
                            timeSeptember += logs.get(i).getTotalSeconds();
                            earnedSeptember += logs.get(i).getMoneyEarned();
                            break;
                        case "October":
                            timeOctober += logs.get(i).getTotalSeconds();
                            earnedOctober += logs.get(i).getMoneyEarned();
                            break;
                        case "November":
                            timeNovember += logs.get(i).getTotalSeconds();
                            earnedNovember += logs.get(i).getMoneyEarned();
                            break;
                        case "December":
                            timeDecember += logs.get(i).getTotalSeconds();
                            earnedDecember += logs.get(i).getMoneyEarned();
                            break;
                    }
                }

                mListStats = new ArrayList<StatsByHolder>();
                mListStats.add(new StatsByHolder("MONTH", "TIME", "EARNED"));
                mListStats.add(new StatsByHolder("January", Utilities.MillisecondsToFormattedElapsedTime(timeJanuary *1000), Utilities.ConvertWageToLocale(getActivity(), earnedJanuary)));
                mListStats.add(new StatsByHolder("February", Utilities.MillisecondsToFormattedElapsedTime(timeFebruary *1000), Utilities.ConvertWageToLocale(getActivity(), earnedFebruary)));
                mListStats.add(new StatsByHolder("March", Utilities.MillisecondsToFormattedElapsedTime(timeMarch *1000), Utilities.ConvertWageToLocale(getActivity(), earnedMarch)));
                mListStats.add(new StatsByHolder("April", Utilities.MillisecondsToFormattedElapsedTime(timeApril *1000), Utilities.ConvertWageToLocale(getActivity(), earnedApril)));
                mListStats.add(new StatsByHolder("May", Utilities.MillisecondsToFormattedElapsedTime(timeMay *1000), Utilities.ConvertWageToLocale(getActivity(), earnedMay)));
                mListStats.add(new StatsByHolder("June", Utilities.MillisecondsToFormattedElapsedTime(timeJune *1000), Utilities.ConvertWageToLocale(getActivity(), earnedJune)));
                mListStats.add(new StatsByHolder("July", Utilities.MillisecondsToFormattedElapsedTime(timeJuly *1000), Utilities.ConvertWageToLocale(getActivity(), earnedJuly)));
                mListStats.add(new StatsByHolder("August", Utilities.MillisecondsToFormattedElapsedTime(timeAugust *1000), Utilities.ConvertWageToLocale(getActivity(), earnedAugust)));
                mListStats.add(new StatsByHolder("September", Utilities.MillisecondsToFormattedElapsedTime(timeSeptember *1000), Utilities.ConvertWageToLocale(getActivity(), earnedSeptember)));
                mListStats.add(new StatsByHolder("October", Utilities.MillisecondsToFormattedElapsedTime(timeOctober *1000), Utilities.ConvertWageToLocale(getActivity(), earnedOctober)));
                mListStats.add(new StatsByHolder("November", Utilities.MillisecondsToFormattedElapsedTime(timeNovember *1000), Utilities.ConvertWageToLocale(getActivity(), earnedNovember)));
                mListStats.add(new StatsByHolder("December", Utilities.MillisecondsToFormattedElapsedTime(timeDecember *1000), Utilities.ConvertWageToLocale(getActivity(), earnedDecember)));

                mAdapter = new DailyStatsAdapter(getActivity(), R.layout.list_simple, mListStats);
                lvStats.setAdapter(mAdapter);

                dismissProgressBar();
            }
        });

        return mView;
    }

    public void showProgressBar(String msg){
        mDialog = ProgressDialog.show(getActivity(), "", msg, true);
    }

    public void dismissProgressBar(){
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }
}
