package com.poopsalary.fragments;

import java.util.List;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.poopsalary.R;
import com.poopsalary.activities.DepositReceiptActivity;
import com.poopsalary.activities.JobEditorActivity;
import com.poopsalary.adapters.LogsParseQueryAdapter;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

public class ParseDepositsFragment extends ListFragment {

    private LogsParseQueryAdapter mAdapter;
    Context mContext;
    Listener mListener = null;
    Dialog mDialog;

    public interface Listener {
        public void onSharePoop();
        public void onPoopRated();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        mContext = getActivity();
        View rootView = inflater.inflate(R.layout.list_simple, container, false);

        mAdapter = new LogsParseQueryAdapter(mContext);
        mAdapter.loadObjects();
        setListAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Logs log = mAdapter.getItem(position);

        Intent intent = new Intent(getActivity(), DepositReceiptActivity.class);
        intent.putExtra("logId", log.getObjectId());
        startActivityForResult(intent, 9999);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == 9999) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                boolean shouldIncrementRating = data.getBooleanExtra("ratingUpdated", false);
                if(shouldIncrementRating){
                    mListener.onPoopRated();
                }

                boolean poopShared = data.getBooleanExtra("sharedPoop", false);
                if(poopShared){
                    mListener.onSharePoop();
                }
            }

            mAdapter.notifyDataSetChanged();
            mAdapter.loadObjects();
            setListAdapter(mAdapter);
        }
    }

    public void setListener(Listener l) {
        mListener = l;
    }

    public void onResume() {
        super.onResume();
        mAdapter.loadObjects();
        setListAdapter(mAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
    }
}