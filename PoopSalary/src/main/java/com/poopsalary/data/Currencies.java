package com.poopsalary.data;

import com.poopsalary.utils.KeyValuePair;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennisfuller on 3/18/14.
 */
public class Currencies {

    private List<KeyValuePair> currencies;

    public List<KeyValuePair> getCurrencies()
    {
        return currencies;
    }

    public Currencies()
    {
        currencies = new ArrayList<KeyValuePair>();

        currencies.add(new KeyValuePair("AED", "United Arab Emirates Dirham"));
        currencies.add(new KeyValuePair("AFN", "Afghan Afghani"));
        currencies.add(new KeyValuePair("ALL", "Albanian Lek"));
        currencies.add(new KeyValuePair("AMD", "Armenian Dram"));
        currencies.add(new KeyValuePair("ANG", "Netherlands Antillean Guilder"));
        currencies.add(new KeyValuePair("AOA", "Angolan Kwanza"));
        currencies.add(new KeyValuePair("ARS", "Argentine Peso"));
        currencies.add(new KeyValuePair("AUD", "Australian Dollar"));
        currencies.add(new KeyValuePair("AWG", "Aruban Florin" ));
        currencies.add(new KeyValuePair("AZN", "Azerbaijani Manat"));
        currencies.add(new KeyValuePair("BAM", "Bosnia-Herzegovina Convertible Mark"));
        currencies.add(new KeyValuePair("BBD", "Barbadian Dollar"));
        currencies.add(new KeyValuePair("BDT", "Bangladeshi Taka"));
        currencies.add(new KeyValuePair("BGN", "Bulgarian Lev"));
        currencies.add(new KeyValuePair("BHD", "Bahraini Dinar"));
        currencies.add(new KeyValuePair("BIF", "Burundian Franc"));
        currencies.add(new KeyValuePair("BMD", "Bermudan Dollar"));
        currencies.add(new KeyValuePair("BND", "Brunei Dollar"));
        currencies.add(new KeyValuePair("BOB", "Bolivian Boliviano"));
        currencies.add(new KeyValuePair("BRL", "Brazilian Real"));
        currencies.add(new KeyValuePair("BSD", "Bahamian Dollar"));
        currencies.add(new KeyValuePair("BTC", "Bitcoin"));
        currencies.add(new KeyValuePair("BTN", "Bhutanese Ngultrum"));
        currencies.add(new KeyValuePair("BWP", "Botswanan Pula"));
        currencies.add(new KeyValuePair("BYR", "Belarusian Ruble"));
        currencies.add(new KeyValuePair("BZD", "Belize Dollar"));
        currencies.add(new KeyValuePair("CAD", "Canadian Dollar"));
        currencies.add(new KeyValuePair("CDF", "Congolese Franc"));
        currencies.add(new KeyValuePair("CHF", "Swiss Franc"));
        currencies.add(new KeyValuePair("CLF", "Chilean Unit of Account (UF)"));
        currencies.add(new KeyValuePair("CLP", "Chilean Peso"));
        currencies.add(new KeyValuePair("CNY", "Chinese Yuan"));
        currencies.add(new KeyValuePair("COP", "Colombian Peso"));
        currencies.add(new KeyValuePair("CRC", "Costa Rican Colón"));
        currencies.add(new KeyValuePair("CUP", "Cuban Peso"));
        currencies.add(new KeyValuePair("CVE", "Cape Verdean Escudo"));
        currencies.add(new KeyValuePair("CZK", "Czech Republic Koruna"));
        currencies.add(new KeyValuePair("DJF", "Djiboutian Franc"));
        currencies.add(new KeyValuePair("DKK", "Danish Krone"));
        currencies.add(new KeyValuePair("DOP", "Dominican Peso"));
        currencies.add(new KeyValuePair("DZD", "Algerian Dinar"));
        currencies.add(new KeyValuePair("EEK", "Estonian Kroon"));
        currencies.add(new KeyValuePair("EGP", "Egyptian Pound"));
        currencies.add(new KeyValuePair("ERN", "Eritrean Nakfa"));
        currencies.add(new KeyValuePair("ETB", "Ethiopian Birr"));
        currencies.add(new KeyValuePair("EUR", "Euro"));
        currencies.add(new KeyValuePair("FJD", "Fijian Dollar"));
        currencies.add(new KeyValuePair("FKP", "Falkland Islands Pound"));
        currencies.add(new KeyValuePair("GBP", "British Pound Sterling"));
        currencies.add(new KeyValuePair("GEL", "Georgian Lari"));
        currencies.add(new KeyValuePair("GHS", "Ghanaian Cedi"));
        currencies.add(new KeyValuePair("GIP", "Gibraltar Pound"));
        currencies.add(new KeyValuePair("GMD", "Gambian Dalasi"));
        currencies.add(new KeyValuePair("GNF", "Guinean Franc"));
        currencies.add(new KeyValuePair("GTQ", "Guatemalan Quetzal"));
        currencies.add(new KeyValuePair("GYD", "Guyanaese Dollar"));
        currencies.add(new KeyValuePair("HKD", "Hong Kong Dollar"));
        currencies.add(new KeyValuePair("HNL", "Honduran Lempira"));
        currencies.add(new KeyValuePair("HRK", "Croatian Kuna"));
        currencies.add(new KeyValuePair("HTG", "Haitian Gourde"));
        currencies.add(new KeyValuePair("HUF", "Hungarian Forint"));
        currencies.add(new KeyValuePair("IDR", "Indonesian Rupiah"));
        currencies.add(new KeyValuePair("ILS", "Israeli New Sheqel"));
        currencies.add(new KeyValuePair("INR", "Indian Rupee"));
        currencies.add(new KeyValuePair("IQD", "Iraqi Dinar"));
        currencies.add(new KeyValuePair("IRR", "Iranian Rial"));
        currencies.add(new KeyValuePair("ISK", "Icelandic Króna"));
        currencies.add(new KeyValuePair("JEP", "Jersey Pound"));
        currencies.add(new KeyValuePair("JMD", "Jamaican Dollar"));
        currencies.add(new KeyValuePair("JOD", "Jordanian Dinar"));
        currencies.add(new KeyValuePair("JPY", "Japanese Yen"));
        currencies.add(new KeyValuePair("KES", "Kenyan Shilling"));
        currencies.add(new KeyValuePair("KGS", "Kyrgystani Som"));
        currencies.add(new KeyValuePair("KHR", "Cambodian Riel"));
        currencies.add(new KeyValuePair("KMF", "Comorian Franc"));
        currencies.add(new KeyValuePair("KPW", "North Korean Won"));
        currencies.add(new KeyValuePair("KRW", "South Korean Won"));
        currencies.add(new KeyValuePair("KWD", "Kuwaiti Dinar"));
        currencies.add(new KeyValuePair("KYD", "Cayman Islands Dollar"));
        currencies.add(new KeyValuePair("KZT", "Kazakhstani Tenge"));
        currencies.add(new KeyValuePair("LAK", "Laotian Kip"));
        currencies.add(new KeyValuePair("LBP", "Lebanese Pound"));
        currencies.add(new KeyValuePair("LKR", "Sri Lankan Rupee"));
        currencies.add(new KeyValuePair("LRD", "Liberian Dollar"));
        currencies.add(new KeyValuePair("LSL", "Lesotho Loti"));
        currencies.add(new KeyValuePair("LTL", "Lithuanian Litas"));
        currencies.add(new KeyValuePair("LVL", "Latvian Lats"));
        currencies.add(new KeyValuePair("LYD", "Libyan Dinar"));
        currencies.add(new KeyValuePair("MAD", "Moroccan Dirham"));
        currencies.add(new KeyValuePair("MDL", "Moldovan Leu"));
        currencies.add(new KeyValuePair("MGA", "Malagasy Ariary"));
        currencies.add(new KeyValuePair("MKD", "Macedonian Denar"));
        currencies.add(new KeyValuePair("MMK", "Myanma Kyat"));
        currencies.add(new KeyValuePair("MNT", "Mongolian Tugrik"));
        currencies.add(new KeyValuePair("MOP", "Macanese Pataca"));
        currencies.add(new KeyValuePair("MRO", "Mauritanian Ouguiya"));
        currencies.add(new KeyValuePair("MTL", "Maltese Lira"));
        currencies.add(new KeyValuePair("MUR", "Mauritian Rupee"));
        currencies.add(new KeyValuePair("MVR", "Maldivian Rufiyaa"));
        currencies.add(new KeyValuePair("MWK", "Malawian Kwacha"));
        currencies.add(new KeyValuePair("MXN", "Mexican Peso"));
        currencies.add(new KeyValuePair("MYR", "Malaysian Ringgit"));
        currencies.add(new KeyValuePair("MZN", "Mozambican Metical"));
        currencies.add(new KeyValuePair("NAD", "Namibian Dollar"));
        currencies.add(new KeyValuePair("NGN", "Nigerian Naira"));
        currencies.add(new KeyValuePair("NIO", "Nicaraguan Córdoba"));
        currencies.add(new KeyValuePair("NOK", "Norwegian Krone"));
        currencies.add(new KeyValuePair("NPR", "Nepalese Rupee"));
        currencies.add(new KeyValuePair("NZD", "New Zealand Dollar"));
        currencies.add(new KeyValuePair("OMR", "Omani Rial"));
        currencies.add(new KeyValuePair("PAB", "Panamanian Balboa"));
        currencies.add(new KeyValuePair("PEN", "Peruvian Nuevo Sol"));
        currencies.add(new KeyValuePair("PGK", "Papua New Guinean Kina"));
        currencies.add(new KeyValuePair("PHP", "Philippine Peso"));
        currencies.add(new KeyValuePair("PKR", "Pakistani Rupee"));
        currencies.add(new KeyValuePair("PLN", "Polish Zloty"));
        currencies.add(new KeyValuePair("PYG", "Paraguayan Guarani"));
        currencies.add(new KeyValuePair("QAR", "Qatari Rial"));
        currencies.add(new KeyValuePair("RON", "Romanian Leu"));
        currencies.add(new KeyValuePair("RSD", "Serbian Dinar"));
        currencies.add(new KeyValuePair("RUB", "Russian Ruble"));
        currencies.add(new KeyValuePair("RWF", "Rwandan Franc"));
        currencies.add(new KeyValuePair("SAR", "Saudi Riyal"));
        currencies.add(new KeyValuePair("SBD", "Solomon Islands Dollar"));
        currencies.add(new KeyValuePair("SCR", "Seychellois Rupee"));
        currencies.add(new KeyValuePair("SDG", "Sudanese Pound"));
        currencies.add(new KeyValuePair("SEK", "Swedish Krona"));
        currencies.add(new KeyValuePair("SGD", "Singapore Dollar"));
        currencies.add(new KeyValuePair("SHP", "Saint Helena Pound"));
        currencies.add(new KeyValuePair("SLL", "Sierra Leonean Leone"));
        currencies.add(new KeyValuePair("SOS", "Somali Shilling"));
        currencies.add(new KeyValuePair("SRD", "Surinamese Dollar"));
        currencies.add(new KeyValuePair("STD", "São Tomé and Príncipe Dobra"));
        currencies.add(new KeyValuePair("SVC", "Salvadoran Colón"));
        currencies.add(new KeyValuePair("SYP", "Syrian Pound"));
        currencies.add(new KeyValuePair("SZL", "Swazi Lilangeni"));
        currencies.add(new KeyValuePair("THB", "Thai Baht"));
        currencies.add(new KeyValuePair("TJS", "Tajikistani Somoni"));
        currencies.add(new KeyValuePair("TMT", "Turkmenistani Manat"));
        currencies.add(new KeyValuePair("TND", "Tunisian Dinar"));
        currencies.add(new KeyValuePair("TOP", "Tongan Paʻanga"));
        currencies.add(new KeyValuePair("TRY", "Turkish Lira"));
        currencies.add(new KeyValuePair("TTD", "Trinidad and Tobago Dollar"));
        currencies.add(new KeyValuePair("TWD", "New Taiwan Dollar"));
        currencies.add(new KeyValuePair("TZS", "Tanzanian Shilling"));
        currencies.add(new KeyValuePair("UAH", "Ukrainian Hryvnia"));
        currencies.add(new KeyValuePair("UGX", "Ugandan Shilling"));
        currencies.add(new KeyValuePair("USD", "United States Dollar"));
        currencies.add(new KeyValuePair("UYU", "Uruguayan Peso"));
        currencies.add(new KeyValuePair("UZS", "Uzbekistan Som"));
        currencies.add(new KeyValuePair("VEF", "Venezuelan Bolívar Fuerte"));
        currencies.add(new KeyValuePair("VND", "Vietnamese Dong"));
        currencies.add(new KeyValuePair("VUV", "Vanuatu Vatu"));
        currencies.add(new KeyValuePair("WST", "Samoan Tala"));
        currencies.add(new KeyValuePair("XAF", "CFA Franc BEAC"));
        currencies.add(new KeyValuePair("XAG", "Silver (troy ounce)"));
        currencies.add(new KeyValuePair("XAU", "Gold (troy ounce)"));
        currencies.add(new KeyValuePair("XCD", "East Caribbean Dollar"));
        currencies.add(new KeyValuePair("XDR", "Special Drawing Rights"));
        currencies.add(new KeyValuePair("XOF", "CFA Franc BCEAO"));
        currencies.add(new KeyValuePair("XPF", "CFP Franc"));
        currencies.add(new KeyValuePair("YER", "Yemeni Rial"));
        currencies.add(new KeyValuePair("ZAR", "South African Rand"));
        currencies.add(new KeyValuePair("ZMK", "Zambian Kwacha (pre-2013)"));
        currencies.add(new KeyValuePair("ZMW", "Zambian Kwacha"));
        currencies.add(new KeyValuePair("ZWL", "Zimbabwean Dollar"));

    }
}