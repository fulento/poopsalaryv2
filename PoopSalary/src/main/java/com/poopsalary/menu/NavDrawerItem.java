package com.poopsalary.menu;

public class NavDrawerItem {

    private String title;
    private int icon;
    // boolean to set whether item is a header
    private boolean isHeader = false;

    public NavDrawerItem(){}

    public NavDrawerItem(String title, int icon){
        this.title = title;
        this.icon = icon;
    }

    public NavDrawerItem(String title, int icon, boolean isHeader){
        this.title = title;
        this.icon = icon;
        this.isHeader = isHeader;
    }

    public String getTitle(){
        return this.title;
    }

    public int getIcon(){
        return this.icon;
    }


    public boolean getIsHeader(){
        return this.isHeader;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setIcon(int icon){
        this.icon = icon;
    }


    public void setIsHeader(boolean isHeader){
        this.isHeader = isHeader;
    }
}
