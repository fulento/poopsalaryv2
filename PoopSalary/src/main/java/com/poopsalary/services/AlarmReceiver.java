package com.poopsalary.services;

import android.content.Context;
import android.content.BroadcastReceiver;
import android.widget.Toast;
import android.content.Intent;


import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.poopsalary.controllers.UserController;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import com.poopsalary.models.StopWatchModel;
import com.poopsalary.utils.AppPreferences;

import java.util.Locale;

/**
 * Created by dennisfuller on 3/11/14.
 */
public class AlarmReceiver extends BroadcastReceiver {

    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {

        mContext = context;

        ParseObject.registerSubclass(Logs.class);
        ParseObject.registerSubclass(Jobs.class);

        try {
            Parse.initialize(new Parse.Configuration.Builder(context)
                    .applicationId("2xnh1xMIEwErXd1xgo73uy52y8QXSsQxjLauLGuf")
                    .clientKey("Tw7MILkNfLTNTasUT32AOOMBQUYN5eJoEu6TCjlg")
                    .enableLocalDataStore()
                    .server("https://parseapi.back4app.com")
                    .build()
            );
        } catch (Exception ex) {

        }

        ParseQuery query = new ParseQuery(Jobs.class);
        query.fromLocalDatastore();
        query.whereEqualTo("user", ParseUser.getCurrentUser());
        query.whereEqualTo("isSelected", true);

        query.getFirstInBackground(new GetCallback<Jobs>() {
            @Override
            public void done(Jobs job, ParseException e) {
                if(job != null){

                    long totalMillis = System.currentTimeMillis()- AppPreferences.getPoopStartTime(mContext);

                    UserController user = new UserController(mContext, new StopWatchModel(), job, Locale.getDefault());

                    user.getStopWatch().Set(totalMillis);


                    if(isBetween(user.getStopWatch().getMinutes(), 5, 10))
                    {
                        Toast.makeText(mContext, "You've been pooping for over 5 minutes...", Toast.LENGTH_LONG).show();
                    }
                    else if(isBetween(user.getStopWatch().getMinutes(), 10, 15))
                    {
                        //don't do anything until after 15 minutes
                    }
                    else if(isBetween(user.getStopWatch().getMinutes(), 15, 30))
                    {
                        Toast.makeText(mContext, "You've been poopinf for over 15 minutes...", Toast.LENGTH_LONG).show();
                    }
                    else if(isBetween(user.getStopWatch().getMinutes(), 30, 45))
                    {
                        //don't do anything until 45 minutes
                    }
                    else if(isBetween(user.getStopWatch().getMinutes(), 45, 60))
                    {
                        Toast.makeText(mContext, "You ok? You've been pooping for over 45 minutes...", Toast.LENGTH_LONG).show();
                    }
                    else if(user.getStopWatch().getHours() > 1)
                    {
                        Toast.makeText(mContext, "Hello! It's been over an hour. You still here?", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

}
