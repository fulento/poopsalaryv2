package com.poopsalary.daos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.SystemClock;

import com.poopsalary.old.DatabaseHelper;
import com.poopsalary.models.CurrentPoopModel;

public class CurrentPoopDao {

    // Database fields
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private Context mContext;
    private String mTableName = "CurrentPoop";
    private String mPrimaryColumn = "_id";
    private String[] mAllColumns = { "_id", "poopStart" };

    public CurrentPoopDao(Context context) {
        mContext = context;
        dbHelper = new DatabaseHelper(context);
    }

    public CurrentPoopModel Start(CurrentPoopModel poop) {
        ContentValues values = new ContentValues();
        values.put("poopStart", poop.getPoopStart());

        long insertId = database.insert(mTableName, null,
                values);
        Cursor cursor = database.query(mTableName,
                mAllColumns, mPrimaryColumn + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        CurrentPoopModel newPoop = cursorToPoop(cursor);
        cursor.close();
        return newPoop;
    }

    public void Delete(CurrentPoopModel poop) {
        long id = poop.get_id();
        System.out.println("Poop deleted with id: " + id);
        database.delete(mTableName, mPrimaryColumn
                + " = " + id, null);
    }

    public boolean HasPoopBeenStarted()
    {
        String mySql = "SELECT * FROM CurrentPoop";
        Cursor c = database.rawQuery(mySql,null);

        c.moveToFirst();

        if(c.getCount() == 0)
        {
            return false;
        }
        else
            return true;
    }

    public long GetCurrentPoopStartTime()
    {
        String sqlStr = "SELECT _id, poopStart FROM CurrentPoop ORDER BY _id ASC";

        Cursor c = database.rawQuery(sqlStr, null);
        long startTime;

        c.moveToFirst();

        if(c.getCount() > 0)
        {
            startTime = c.getLong(c.getColumnIndex("poopStart"));
        }
        else
            startTime = SystemClock.elapsedRealtime();

        return startTime;
    }

    public void Open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void Close() {
        dbHelper.close();
    }

    private CurrentPoopModel cursorToPoop(Cursor cursor) {
        CurrentPoopModel poop = new CurrentPoopModel();

        if(cursor.getCount() > 0){
            poop.set_id(cursor.getInt(0));
            poop.setPoopStart(cursor.getInt(1));
        }

        return poop;
    }
}
