package com.poopsalary.utils;

import android.content.SharedPreferences;
import android.content.Context;

import com.parse.ParseACL;

import java.util.Date;


public class AppPreferences {

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("PoopSalaryPreferences", Context.MODE_PRIVATE);
    }

    //CONVERSION RATE
    public static double getConversionRate(Context context) {
        return Double.valueOf(Float.toString(getPrefs(context).getFloat("ConversionRate", 1)));
    }

    public static void setConversionRate(Context context, double value){
        getPrefs(context).edit().putFloat("ConversionRate", Float.valueOf(Double.toString(value))).apply();
    }

    public static String getCurrency(Context context) {
        return getPrefs(context).getString("Currency", "USD");
    }

    public static void setCurrency(Context context, String value){
        getPrefs(context).edit().putString("Currency", value).apply();
    }

    //POOP START TIME
    public static long getPoopStartTime(Context context){
        return getPrefs(context).getLong("StartPoopTime", 0);
    }

    public static void setPoopStartTime(Context context, long value){
        getPrefs(context).edit().putLong("StartPoopTime", value).apply();
    }

    //POOP STREAK
    public static int getPoopStreak(Context context){
        return getPrefs(context).getInt("CurrentPoopStreak", 0);
    }

    public static void setPoopStreak(Context context, int value){
        getPrefs(context).edit().putInt("CurrentPoopStreak", value).apply();
    }

    //LAST COMPLETED POOP
    public static long getLastCompletedPoop(Context context){
        return getPrefs(context).getLong("LastCompletedPoop", 0);
    }

    public static void setLastCompletedPoop(Context context, long value){
        getPrefs(context).edit().putLong("LastCompletedPoop", value).apply();
    }

    //FRIEND SHARES
    public static int getFriendShares(Context context){
        return getPrefs(context).getInt("TotalFriendShares", 0);
    }

    public static void setFriendShares(Context context, int value){
        getPrefs(context).edit().putInt("TotalFriendShares", value).apply();
    }

    //APP SHARES
    public static int getAppShares(Context context){
        return getPrefs(context).getInt("TotalAppShares", 0);
    }

    public static void setAppShares(Context context, int value){
        getPrefs(context).edit().putInt("TotalAppShares", value).apply();
    }

    public static String getUserPayload(Context context){
        return getPrefs(context).getString("UserPayLoad", "");
    }

    public static void setUserPayLoad(Context context, String value){
        getPrefs(context).edit().putString("UserPayLoad", value).apply();
    }

    public static boolean getHasLinkedGoogleAccount(Context context){
        return getPrefs(context).getBoolean("HasLinkedGoogleAccount", false);
    }

    public static void setHasLinkedGoogleAccount(Context context, boolean value){
        getPrefs(context).edit().putBoolean("HasLinkedGoogleAccount", value).apply();
    }

    public static boolean getIsFirstRun(Context context)
    {
        return getPrefs(context).getBoolean("IsFirstRun", true);
    }

    public static void setIsFirstRun(Context context, boolean value)
    {
        getPrefs(context).edit().putBoolean("IsFirstRun", value).apply();
    }

    public static long getLastRateCheck(Context context)
    {
        return getPrefs(context).getLong("LastRateCheck", new Date().getTime());
    }

    public static void setLastRateCheck(Context context, long value)
    {
        getPrefs(context).edit().putLong("LastRateCheck", value);
    }


}
