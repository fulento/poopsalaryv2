package com.poopsalary.utils;

import android.content.Context;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.poopsalary.models.ParseExceptions;
import com.poopsalary.models.Logs;
import com.poopsalary.models.StopWatchModel;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;


public class Utilities {

    public static String MillisecondsToFormattedElapsedTime(long milliseconds)
    {
        StopWatchModel sw = new StopWatchModel();
        sw.Set(milliseconds);

        return String.format("%02d:%02d", sw.getMinutes(), sw.getSeconds());
    }

    public static int ConvertEarningsToInt()
    {
        return 0;
    }

    public static String ConvertWageToLocale(Context context, double earnings)
    {
        double parsed = round(earnings * AppPreferences.getConversionRate(context), 2, 1);
        String formato = NumberFormat.getCurrencyInstance(Locale.getDefault()).format((parsed));
        return  formato;
    }

    public static double CalculateWageDouble(String wageType, double wage, long elapsedTime)
    {

        double newEarnings=0;

        if(wageType.equalsIgnoreCase("Hourly"))
        {
            newEarnings = ((wage/60)/60)* elapsedTime;
        }
        else
        {
            newEarnings = ((((wage/52)/40)/60)/60)* elapsedTime;
        }

        return round(newEarnings, 2, 1);

    }

    public static double round(double unrounded, int precision, int roundingMode)
    {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }

    public static long Date_to_MilliSeconds(int day, int month, int year, int hour, int minute)
    {

        Calendar c = Calendar.getInstance();
        c.set(year, month, day, hour, minute, 00);

        return c.getTimeInMillis();

    }

    public static String getDayIntFromDate(long dateInMilliseconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliseconds);
        return String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static String getMonthOfYear(long dateInMilliseconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliseconds);
        return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
    }

    public static String getDayOfWeek(long dateInMilliseconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliseconds);
        return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
    }

    public static String getHourOfDay(long dateInMilliseconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliseconds);
        return String.valueOf(calendar.get(Calendar.HOUR)) + calendar.getDisplayName(Calendar.AM_PM, Calendar.LONG, Locale.getDefault());
    }

    public static String getFullFormattedDate(long dateInMilliseconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliseconds);

        String month =calendar.getDisplayName(Calendar.MONTH,Calendar.SHORT, Locale.getDefault());
        String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        String ampm = calendar.getDisplayName(Calendar.AM_PM, Calendar.SHORT, Locale.getDefault());


        return month + " " + day + ", " + year + " " + String.format("%02d:%02d", hour,minute) + " " + ampm;
    }

    public static String getDateDayMonthYear(long dateInMilliseconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliseconds);

        String month =calendar.getDisplayName(Calendar.MONTH,Calendar.SHORT, Locale.getDefault());
        String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String year = String.valueOf(calendar.get(Calendar.YEAR));

        return month + "/" + day + "/" + year;
    }

    public static String getDateDayMonthYearTime(Date parseDate)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parseDate);

        String month =calendar.getDisplayName(Calendar.MONTH,Calendar.SHORT, Locale.getDefault());
        String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String hour = String.valueOf(calendar.get(Calendar.HOUR));
        String minute = String.valueOf(calendar.get(Calendar.MINUTE));
        return month + "/" + day + "/" + year + " " + hour + ":" + minute;
    }

    public static Date getFullDate(long dateInMilliseconds)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliseconds);

        return calendar.getTime();
    }

    public static String getDate()
    {

        //   Allocates a Date object and initializes it so that it represents the time
        // at which it was allocated, measured to the nearest millisecond.
        Date dateNow = new Date ();

        SimpleDateFormat dateformatMMDDYYYY = new SimpleDateFormat("MM/dd/yyyy");
        String nowMMDDYYYY = dateformatMMDDYYYY.format( dateNow );

        return nowMMDDYYYY;
    }

    public static Date getDateLast7Days()
    {
        Date dateNow = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(dateNow);

        c.add(Calendar.DATE, -7);

        dateNow.setTime(c.getTimeInMillis());
        return dateNow;
    }

    public static Date getDateLastXDays(int days)
    {
        Date dateNow = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(dateNow);

        c.add(Calendar.DATE, -days);

        dateNow.setTime(c.getTimeInMillis());
        return dateNow;
    }

    public static Date getDateLast30Days()
    {
        Date dateNow = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(dateNow);

        c.add(Calendar.DATE, -30);

        dateNow.setTime(c.getTimeInMillis());
        return dateNow;
    }

    public static Date getDateLastYear()
    {
        Date dateNow = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(dateNow);

        c.add(Calendar.DATE, -365);

        dateNow.setTime(c.getTimeInMillis());
        return dateNow;
    }

    public static int getDaysDiffHours(long startDate, long endDate)
    {
        long diffInMs = endDate - startDate;

        long hours = TimeUnit.MILLISECONDS.toHours(diffInMs);

        return (int)hours;

    }

    public static long getAveragePoopTime(List<Logs> logs){

        long totalSeconds = 0;

        for(int i = 0; i < logs.size(); i++){
            totalSeconds += logs.get(i).getTotalSeconds();
        }

        return totalSeconds * 1000 / logs.size();
    }

    public static void PostExceptionToParse(ParseException e)
    {
        ParseExceptions exc = new ParseExceptions();
        exc.putCode(e.getCode());
        exc.putMessage(e.getMessage());
        exc.putStackTrace(e.getStackTrace().toString());
        exc.saveInBackground();
    }

    public static ParseACL getUserParseAcl()
    {
        ParseACL acl = new ParseACL();
        acl.setPublicWriteAccess(false);
        acl.setPublicReadAccess(false);
        acl.setWriteAccess(ParseUser.getCurrentUser(), true);
        acl.setReadAccess(ParseUser.getCurrentUser(), true);
        return acl;
    }

}
