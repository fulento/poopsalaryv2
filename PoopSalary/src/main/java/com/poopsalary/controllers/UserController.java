package com.poopsalary.controllers;

import android.content.Context;

import com.poopsalary.models.Jobs;
import com.poopsalary.models.StopWatchModel;
import com.poopsalary.utils.Utilities;

import java.util.Locale;

/**
 * Created by dennisfuller on 4/6/14.
 */
public class UserController {

    private StopWatchModel stopWatch;
    private Jobs job;
    private Locale locale;
    private Context context;

    public UserController(Context c, StopWatchModel sw, Jobs job, Locale l){
        context = c;
        stopWatch = sw;
        this.job = job;
        locale = l;
    }

    public StopWatchModel getStopWatch(){
        return stopWatch;
    }

    public Jobs getCurrentJob(){
        return job;
    }

    public Double getCalculatedWage(){
        return Utilities.CalculateWageDouble(job.getWageType(), job.getWage() < 1 ? 1 : job.getWage(), stopWatch.getTotalMills() / 1000);
    }

    public String getFormattedMoneyEarned(){
        return Utilities.ConvertWageToLocale(context, getCalculatedWage());
    }

    public int getRoundedCalculatedWage(){
        return (int)Math.round(getCalculatedWage());
    }

    public String getFormattedElapsedTime()
    {
        return Utilities.MillisecondsToFormattedElapsedTime(stopWatch.getTotalMills());
    }
}
