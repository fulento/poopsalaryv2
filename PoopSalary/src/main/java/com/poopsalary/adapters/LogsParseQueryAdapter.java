package com.poopsalary.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.poopsalary.R;
import com.poopsalary.models.Logs;
import com.poopsalary.utils.AppPreferences;
import com.poopsalary.utils.Utilities;

public class LogsParseQueryAdapter extends ParseQueryAdapter<Logs> {

    Context mContext;

    public LogsParseQueryAdapter(Context context){
        super(context, new ParseQueryAdapter.QueryFactory<Logs>(){
            public ParseQuery create(){
                ParseQuery<Logs> query = new ParseQuery<Logs>(Logs.class);
                query.fromLocalDatastore();
                query.whereEqualTo("user", ParseUser.getCurrentUser());
                //query.include("job");
                query.addDescendingOrder("timeStarted");
                return query;
            }
        });

        mContext = context;
    }

    @Override
    public View getItemView(Logs log, View v, ViewGroup parent){
        if(v == null){
            LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);
            v = mLayoutInflater.inflate(R.layout.row_logs, null);
        }

        TextView month = (TextView) v.findViewById(R.id.txtMonth);
        TextView day = (TextView) v.findViewById(R.id.txtDay);
        RatingBar ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
        TextView moneyEarned = (TextView) v.findViewById(R.id.txtMoneyEarned);
        TextView timeElapsed = (TextView) v.findViewById(R.id.txtElapsedTime);

        month.setText(Utilities.getMonthOfYear(log.getTimeCompleted().getTime()));
        day.setText(Utilities.getDayIntFromDate(log.getTimeCompleted().getTime()));
        moneyEarned.setText(Utilities.ConvertWageToLocale(getContext(), log.getMoneyEarned()));
        timeElapsed.setText(Utilities.MillisecondsToFormattedElapsedTime(log.getTotalSeconds() * 1000));
        ratingBar.setRating(log.getRating());

        return v;
    }
}
