package com.poopsalary.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.poopsalary.R;
import com.poopsalary.models.Jobs;
import com.poopsalary.utils.Utilities;

public class JobsParseQueryAdapter extends ParseQueryAdapter<Jobs> {

    public JobsParseQueryAdapter(Context context){
        super(context, new QueryFactory<Jobs>(){
            public ParseQuery create(){
                ParseQuery<Jobs> query = new ParseQuery<Jobs>(Jobs.class);
                query.fromLocalDatastore();
                query.whereEqualTo("user", ParseUser.getCurrentUser());
                return query;
            }
        });
    }

    @Override
    public View getItemView(Jobs job, View v, ViewGroup parent){
        if(v == null){
            LayoutInflater mLayoutInflater = LayoutInflater.from(getContext());
            v = mLayoutInflater.inflate(R.layout.row_jobs, null);
        }

        TextView jobName = (TextView) v.findViewById(R.id.txtJobNameSetting);
        TextView wage = (TextView) v.findViewById(R.id.txtWageSetting);
        TextView wageType = (TextView) v.findViewById(R.id.txtWageTypeSetting);
        TextView isSelected = (TextView) v.findViewById(R.id.txtActiveSetting);

        jobName.setText(job.getJobName());
        wage.setText(Utilities.ConvertWageToLocale(getContext(), job.getWage()));
        wageType.setText(job.getWageType());

        if(job.getIsSelected()){
            isSelected.setText("Active");
        } else {
            isSelected.setText("");
        }

        return v;
    }
}