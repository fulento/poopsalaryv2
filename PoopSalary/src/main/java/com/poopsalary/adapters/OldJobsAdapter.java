package com.poopsalary.adapters;

import com.poopsalary.R;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class OldJobsAdapter extends SimpleCursorAdapter
{
    private int layout;

    public OldJobsAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        this.layout = layout;
        context.getApplicationInfo();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        Cursor c = getCursor();

        final LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(layout, parent, false);

        int logCount = c.getCount();
        TextView txtLogsCount = (TextView) v.findViewById(R.id.txtLogsCount);

        txtLogsCount.setText(Integer.toString(logCount));

        return v;
    }

    @Override
    public void bindView(View v, Context context, Cursor c) {

        int logCount = c.getCount();

        TextView txtLogsCount = (TextView) v.findViewById(R.id.txtLogsCount);

        txtLogsCount.setText(Integer.toString(logCount));
    }

}