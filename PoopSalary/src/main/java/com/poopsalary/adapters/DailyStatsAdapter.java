package com.poopsalary.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.poopsalary.R;

import com.poopsalary.models.StatsByHolder;

import java.util.List;

public class DailyStatsAdapter extends ArrayAdapter<StatsByHolder> {

    public DailyStatsAdapter(Context context, int resource, List<StatsByHolder> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        StatsByHolder stats = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_daily_stats, parent, false);
        }

        // Lookup view for data population
        TextView tvHeader = (TextView) convertView.findViewById(R.id.txtDailyHeader);
        TextView tvTime = (TextView) convertView.findViewById(R.id.txtDailyTime);
        TextView tvEarned = (TextView) convertView.findViewById(R.id.txtDailyEarned);

        // Populate the data into the template view using the data object
        tvHeader.setText(stats.RowName);
        tvTime.setText(stats.ColumnOne);
        tvEarned.setText(stats.ColumnTwo);

        // Return the completed view to render on screen
        return convertView;
    }

}
