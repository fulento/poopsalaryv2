package com.poopsalary.adapters;

import com.poopsalary.R;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class OldLogsAdapter extends SimpleCursorAdapter
{
    private int layout;

    public OldLogsAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        this.layout = layout;
        context.getApplicationInfo();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        Cursor c = getCursor();

        final LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(layout, parent, false);

        int jobCount = c.getCount();
        TextView txtJobsCount = (TextView) v.findViewById(R.id.txtJobsCount);

        txtJobsCount.setText(Integer.toString(jobCount));

        return v;
    }

    @Override
    public void bindView(View v, Context context, Cursor c) {

        int jobCount = c.getCount();
        TextView txtJobsCount = (TextView) v.findViewById(R.id.txtJobsCount);

        txtJobsCount.setText(Integer.toString(jobCount));
    }
}