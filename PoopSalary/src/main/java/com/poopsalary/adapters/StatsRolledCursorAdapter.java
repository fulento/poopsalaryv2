package com.poopsalary.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.poopsalary.R;
import com.poopsalary.utils.Utilities;

/**
 * Created by dennisfuller on 3/29/14.
 */
public class StatsRolledCursorAdapter extends SimpleCursorAdapter {

    Context mContext;

    public StatsRolledCursorAdapter (Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to,flags);
        mContext = context;

        context.getApplicationInfo();
    }

    @Override
    public void setViewText(TextView v, String text) {

        switch(v.getId())
        {
            case R.id.txtRolledCategory:
                text = text;
                break;
            case R.id.txtRolledValue:
                text = text;
                break;
        }

        v.setText(text);
    }
}
