package com.poopsalary;

import android.app.Application;

import com.parse.ParseACL;

public class MyApplication extends Application
{
    private double TotalEarnedWeek;
    private double TotalEarnedMonth;
    private double TotalEarnedYtd;
    private String TotalTimeWeek;
    private String TotalTimeMonth;
    private String TotalTimeYtd;
    private double HighScoreEarned;
    private String HighScoreTimed;
    private long CurrentProfile;
    private ParseACL _parseACL;

    public void set_parseACL(ParseACL parseACL){
        this._parseACL = parseACL;
    }
    public ParseACL get_parseACL(){
        return this._parseACL;
    }
    public void setCurrentProfile(long currentProfile)
    {
        this.CurrentProfile=currentProfile;
    }

    public long getCurrentProfile()
    {
        return CurrentProfile;
    }

    public void setTotalEarnedWeek(double totalEarned)
    {
        this.TotalEarnedWeek = totalEarned;
    }

    public double getTotalEarnedWeek()
    {
        return TotalEarnedWeek;
    }

    public void setTotalEarnedMonth(double totalEarned)
    {
        this.TotalEarnedMonth=totalEarned;
    }

    public double getTotalEarnedMonth()
    {
        return TotalEarnedMonth;
    }

    public void setTotalEarnedYtd(double totalEarned)
    {
        this.TotalEarnedYtd=totalEarned;
    }

    public double getTotalEarnedYtd()
    {
        return TotalEarnedYtd;
    }

    public void setHighScoreTimed(String totalTime)
    {
        this.HighScoreTimed = totalTime;
    }

    public String getHighScoreTimed()
    {
        return HighScoreTimed;
    }

    public void setHighScoreEarned(double timeElapsed)
    {
        this.HighScoreEarned = timeElapsed;
    }

    public double getHighScoreEarned()
    {
        return HighScoreEarned;
    }

    public void setTotalTimeWeek(String timeElapsed)
    {
        this.TotalTimeWeek = timeElapsed;
    }

    public String getTotalTimeWeek()
    {
        return TotalTimeWeek;
    }

    public void setTotalTimeMonth(String timeElapsed)
    {
        this.TotalTimeMonth = timeElapsed;
    }

    public String getTotalTimeMonth()
    {
        return TotalTimeMonth;
    }

    public void setTotalTimeYtd(String timeElapsed)
    {
        this.TotalTimeYtd = timeElapsed;
    }

    public String getTotalTimeYtd()
    {
        return TotalTimeYtd;
    }
}
