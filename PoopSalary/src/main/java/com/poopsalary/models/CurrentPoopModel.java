package com.poopsalary.models;

/// <summary>
/// TABLE: CurrentPoop / COLUMNS: _id(int), poopStart(num)
/// </summary>
public class CurrentPoopModel {
    private int _id;
    private long poopStart;

    //GETTERS
    public int get_id(){return _id;}
    public long getPoopStart(){return poopStart;}

    //SETTERS
    public void set_id(int _id) {this._id = _id;}
    public void setPoopStart(long poopStart){this.poopStart = poopStart;}
}
