package com.poopsalary.models;

/// <summary>
/// TABLE: Settings / COLUMNS: _id(int), wageType(text), wage(num)
/// </summary>
public class SettingsModel {

    private int _id;
    private String wageType;
    private double wage;

    //GETTERS
    public int get_id(){return _id;}
    public String getWageType(){return wageType;}
    public double getWage(){return wage;}

    //SETTERS
    public void set_id(int _id){this._id = _id;}
    public void setWageType(String wageType){this.wageType = wageType;}
    public void setWage(double wage){this.wage = wage;}
}
