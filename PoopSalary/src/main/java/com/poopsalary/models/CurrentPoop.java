package com.poopsalary.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.Date;

@ParseClassName("CurrentPoop")
public class CurrentPoop extends ParseObject{
    public CurrentPoop(){

    }

    public Date getTimeStarted(){
        return getDate("timeStarted");
    }
    public void setTimeStarted(Date timeStarted){
        put("timeStarted", timeStarted);
    }

    public ParseUser getUser(){
        return ParseUser.getCurrentUser();
    }

    public void setUser(ParseUser user){
        put("user", user);
    }
}