package com.poopsalary.models;

/// <summary>
/// TABLE: Profiles / COLUMNS: _id(int), profileId(int)
/// </summary>
public class CurrentProfileModel {
    private int _id;
    private int profileId;

    //GETTERS
    public int get_id(){
        return _id;
    }
    public int getProfileId(){return profileId;}

    //SETTERS
    public void set_id(int _id){
        this._id = _id;
    }
    public void setProfileId(int profileId){
        this.profileId = profileId;
    }
}
