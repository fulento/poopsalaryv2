package com.poopsalary.models;

/**
 * Created by dennisfuller on 3/26/14.
 */
public class StatsAdapterModel {
    private String category;
    private int value;

    public String getCategory(){
        return category;
    }

    public void setCategory(String value){
        this.category = value;
    }

    public int getValue(){
        return value;
    }

    public void setValue(int value){
        this.value = value;
    }
}
