package com.poopsalary.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Exceptions")
public class ParseExceptions extends ParseObject {

    public int getCode(){ return getInt("code");}
    public String getMessage() { return getString("message");}
    public String getStackTrace() { return getString("stackTrace");}

    public void putCode(int code) { put("code", code);}
    public void putMessage(String message) { put("message", message); }
    public void putStackTrace(String stackTrace) { put("stackTrace", stackTrace);}
}
