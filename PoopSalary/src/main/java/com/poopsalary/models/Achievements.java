package com.poopsalary.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Achievements")
public class Achievements extends ParseObject {

    public String getAchievementName(){
        return getString("achievementName");
    }
    public void setAchievementName(String achievementName){
        put("achievementName", achievementName);
    }
}
