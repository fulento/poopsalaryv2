package com.poopsalary.models;

public class StatsByHolder {

    public String RowName;
    public String ColumnOne;
    public String ColumnTwo;

    public StatsByHolder(String rowName, String columnOne, String columnTwo)
    {
        RowName = rowName;
        ColumnOne = columnOne;
        ColumnTwo = columnTwo;
    }
}
