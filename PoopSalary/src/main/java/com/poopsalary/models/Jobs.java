package com.poopsalary.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.poopsalary.utils.ParseProxyObject;
import com.poopsalary.utils.Utilities;

import java.io.Serializable;
import java.util.Date;

@ParseClassName("Jobs")
public class Jobs extends ParseObject {

    public Jobs() {

    }

    public static ParseQuery<Jobs> getQuery(){
        return ParseQuery.getQuery(Jobs.class);
    }

    public double getWage() {
        return getDouble("wage");
    }
    public String getWageType() {
        return getString("wageType");
    }
    public String getJobName() {
        return getString("jobName");
    }
    public boolean getIsSelected() {
        return getBoolean("isSelected");
    }
    public String getCurrency() { return getString("currency"); }

    public void setWage(double wage) {
        put("wage", wage);
    }
    public void setWageType(String wageType) {
        put("wageType", wageType);
    }
    public void setJobName(String jobName) {
        put("jobName", jobName);
    }
    public void setIsSelected(boolean isSelected) {
        put("isSelected", isSelected);
    }
    public void setCurrency(String currency) {
        put("currency", currency);
    }

    //POINTERS and RELATIONS

    public ParseUser getUser() {
        return ParseUser.getCurrentUser();
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }

    public ParseACL getAcl() { return getACL();}
    public void setACL(){
        put("ACL", Utilities.getUserParseAcl());
    }

}