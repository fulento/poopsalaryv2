package com.poopsalary.models;

/// <summary>
/// TABLE: Profiles / COLUMNS: _id(int), profileName(text), wageType(text), wage(num), beingUsed(text)
/// </summary>
public class ProfilesModel {

    private int _id;
    private String profileName;
    private String wageType;
    private double wage;
    private String beingUsed;

    //GETTERS
    public int get_id(){return _id;}
    public String getProfileName(){return profileName;}
    public String getWageType(){return wageType;}
    public double getWage(){return wage;}
    public String getBeingUsed(){return beingUsed;}

    //SETTERS
    public void set_id(int _id){this._id = _id;}
    public void setProfileName(String profileName){this.profileName = profileName;}
    public void setWageType(String wageType){this.wageType = wageType;}
    public void setWage(double wage){this.wage = wage;}
    public void setBeingUsed(String beingUsed){this.beingUsed = beingUsed;}




}
