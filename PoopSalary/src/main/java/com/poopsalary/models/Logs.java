package com.poopsalary.models;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.poopsalary.utils.ParseProxyObject;
import com.poopsalary.utils.Utilities;

import java.io.Serializable;
import java.util.Date;

@ParseClassName("Logs")
public class Logs extends ParseObject implements Serializable{
    public Logs(){

    }

    //GETTERS
    public Date getTimeCompleted(){
        return getDate("timeCompleted");
    }
    public Date getTimeStarted() {
        return getDate("timeStarted");
    }
    public boolean getHasBeenEdited(){
        return getBoolean("hasBeenEdited");
    }
    public double getMoneyEarned(){
        return getDouble("moneyEarned");
    }
    public float getRating(){
        return getInt("rating");
    }
    public long getTotalSeconds(){
        return getLong("totalSeconds");
    }
    public boolean getIsNew() { return getBoolean("isNew"); }
    public ParseACL getAcl() {
        return getACL();
    }

    //SETTERS
    public void setTimeCompleted(Date timeCompleted){
        put("timeCompleted", timeCompleted);
    }
    public void setTimeStarted(Date timeStarted){
        put("timeStarted", timeStarted);
    }
    public void setHasBeenEdited(boolean hasBeenEdited){
        put("hasBeenEdited", hasBeenEdited);
    }
    public void setMoneyEarned(double moneyEarned){
        put("moneyEarned", moneyEarned);
    }
    public void setRating(float rating){
        put("rating", rating);
    }
    public void setACL() {
        put("ACL", Utilities.getUserParseAcl());
    }
    public void setTotalSeconds(long totalSeconds){
        put("totalSeconds", totalSeconds);
    }
    public void setIsNew(boolean isNew) { put("isNew", isNew); }
    public static ParseQuery<Logs> getQuery(){
        return ParseQuery.getQuery(Logs.class);
    }

    //POINTERS and RELATIONS

    public Jobs getJob() {
        return (Jobs)getParseObject("job");
    }
    public void setJob(Jobs job) {
        put("job", job);
    }

    public ParseUser getUser(){
        return ParseUser.getCurrentUser();
    }
    public void setUser(ParseUser user){
        put("user", user);
    }



}


