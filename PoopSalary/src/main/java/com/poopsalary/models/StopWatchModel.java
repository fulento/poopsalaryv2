package com.poopsalary.models;

import com.poopsalary.utils.Utilities;

public class StopWatchModel {

    private int seconds;
    private int minutes;
    private int hours;
    private long totalMills;

    //GETTERS
    public int getSeconds(){return seconds;}
    public int getMinutes(){return minutes;}
    public int getHours(){return hours;}
    public long getTotalMills(){return totalMills;}

    //SETTERS

    public void Set(long totalMills)
    {
        this.totalMills = totalMills;
        this.seconds = (int) (totalMills / 1000) % 60;
        this.minutes = (int) ((totalMills / (1000*60)) % 60);
        this.hours = (int) ((totalMills / (1000*60*60)) % 24);

    }

    public void Reset()
    {
        seconds = 0;
        minutes = 0;
        hours = 0;
        totalMills = 0;
    }

    public String getFormattedTime()
    {
        return Utilities.MillisecondsToFormattedElapsedTime(totalMills);
    }

    public StopWatchModel(){}

}
