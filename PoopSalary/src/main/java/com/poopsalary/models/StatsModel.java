package com.poopsalary.models;

import com.poopsalary.utils.Utilities;

/// <summary>
/// TABLE: Stats / COLUMNS: _id(int), dateEntered(int), timeElapsed(int), moneyEarned(num), ratings(num)
/// </summary>
public class StatsModel {

    private int _id;
    private long dateEntered;
    private long timeElapsed;
    private double moneyEarned;
    private long ratings;

    //GETTERS
    public int get_id(){return _id;}
    public long getDateEntered(){return dateEntered;}
    public long getTimeElapsed(){return timeElapsed;}
    public double getMoneyEarned(){return moneyEarned;}
    public long getRatings(){return ratings;}

    //SETTERS
    public void set_id(int _id){this._id = _id;}
    public void setDateEntered(long dateEntered){this.dateEntered = dateEntered;}
    public void setTimeElapsed(long timeElapsed){this.timeElapsed = timeElapsed;}
    public void setMoneyEarned(double moneyEarned){this.moneyEarned = moneyEarned;}
    public void setRatings(long ratings){this.ratings = ratings;}

}
