package com.poopsalary.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("ExchangeRates")
public class ExchangeRates extends ParseObject {

    public ExchangeRates(){
    }

    public String getSymbol(){
        return getString("symbol");
    }
    public double getRate(){ return getDouble("rate"); }

    public void setSymbol(String symbol){ put("symbol", symbol); }
    public void setRate(double rate){put("rate", rate); }

}
