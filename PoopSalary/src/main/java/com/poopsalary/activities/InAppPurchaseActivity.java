
package com.poopsalary.activities;

import java.util.ArrayList;
import java.util.List;


import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.poopsalary.utils.AppPreferences;
import com.poopsalary.utils.IabHelper;

import com.poopsalary.utils.IabResult;
import com.poopsalary.utils.Inventory;
import com.poopsalary.utils.Purchase;
import com.poopsalary.utils.SkuDetails;

import com.poopsalary.R;
import com.poopsalary.utils.Utilities;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class InAppPurchaseActivity extends Activity {
    public static final String TAG = "InAppActivity";
    private Button btnIapCancel;
    private Button btnIapRate;
    private TextView txtStatusIap;

    private Button mBtnBuyAchievements;
    private Button mBtnBuyLeaderboards;
    private Button mBtnDisableAds;
    private Button mBtnBuyAllFeatures;

    private TextView mTxtBuyAchievements;
    private TextView mTxtBuyLeaderboards;
    private TextView mTxtDisableAds;
    private TextView mTxtBuyAllFeatures;

    // The helper object
    IabHelper mHelper;

    String base64EncodedPublicKey = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.poopsalary.R.layout.activity_in_app);
        base64EncodedPublicKey=getResources().getString(com.poopsalary.R.string.google_base64EncodedPublicKey);
        Log.d(TAG, "google base64 encoded "+base64EncodedPublicKey);

        mBtnBuyAllFeatures = (Button) findViewById(R.id.btnBuyAllFeatures);
        mBtnBuyAllFeatures.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                launchPurchaseFlow(getResources().getString(R.string.unlockpro));
            }
        });

        if(ParseUser.getCurrentUser().get("paid").equals(true)){
            mBtnBuyAllFeatures.setEnabled(false);
        }

        disableSkuInterfaces();

        btnIapCancel=(Button) findViewById(R.id.btn_iap_cancel);
        btnIapCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ParseUser.getCurrentUser().put("hasGoogleAccount", false);
                ParseUser.getCurrentUser().put("googleSignedIn", false);
                ParseUser.getCurrentUser().put("googleId", "");
                AppPreferences.setHasLinkedGoogleAccount(getApplicationContext(), false);
                ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                });
            }
        });

        setWaitScreen(true);
        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");
                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d(TAG,"Problem setting up in-app billing: " + result);
                    return;
                }
                List<String> additionalSkuList = new ArrayList<String>();
                additionalSkuList.add(getResources().getString(R.string.unlockpro));

                mHelper.queryInventoryAsync(true, additionalSkuList,
                        mQueryFinishedListener);

            }
        });
    }

    protected void launchPurchaseFlow(String sku) {
        setWaitScreen(true);
        String payload = AppPreferences.getUserPayload(getContext());

        try{
            mHelper.launchPurchaseFlow(this, sku, 10022,
                    mPurchaseFinishedListener, payload);
        }catch(IllegalStateException ise){
            //just wait to finish other tasks
            Toast.makeText(this, "Waiting for market interrogation to complete . . .", Toast.LENGTH_SHORT).show();
            setWaitScreen(false);
            Log.e(TAG, ise.getMessage());
        }
    }

    /**
     * When the application resumes the application checks which customer is signed in.
     */
    @Override
    protected void onResume() {
        super.onResume();
        setWaitScreen(false);
    };

    @Override
    public void onBackPressed() {
        setResult(9999);
        finish();

    }

    protected InAppPurchaseActivity getContext(){
        return this;
    }

    // We're being destroyed. It's important to dispose of the helper here!
    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }

    }

    /**
     * Checks if network is available
     * @return boolean
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (mHelper == null) return;

            if (result.isFailure()) {

                setWaitScreen(false);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {

                setWaitScreen(false);
                return;
            }

            //if sku
            if (purchase.getSku().equals(getResources().getString(R.string.unlockpro))) {
                //consume it 
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }

        }


    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            if (mHelper == null) return;
            //check sku
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit

                String sku=purchase.getSku();
                //increment coins
                int coins;
                if (sku.equalsIgnoreCase(getResources().getString(R.string.unlockpro))){
                    ParseUser.getCurrentUser().put("paid", true);
                    ParseUser.getCurrentUser().saveEventually();
                    mBtnBuyAllFeatures.setEnabled(false);
                    setResult(RESULT_OK);
                    finish();
                }
            }
            else {
                Log.e(TAG,"Error while consuming: " + result);
            }

            setWaitScreen(false);

        }

    };

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            if (mHelper == null) return;
            if (result.isFailure()) {

                setWaitScreen(false);
                return;
            }

            Purchase allFeaturesPurchase = inventory.getPurchase(getResources().getString(R.string.unlockpro));
            if (allFeaturesPurchase != null && verifyDeveloperPayload(allFeaturesPurchase)) {

                mHelper.consumeAsync(inventory.getPurchase(getResources().getString(R.string.unlockpro)), mConsumeFinishedListener);
                return;
            }

            setWaitScreen(false);

        }
    };

    // Listener that's called when we finish querying for items details
    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            if (mHelper == null) return;
            if (result.isFailure()) {

                setWaitScreen(false);
                return;
            }

            SkuDetails allFeatures = inventory.getSkuDetails(getResources().getString(R.string.unlockpro));
            if(allFeatures!=null) {

                enableSkuInterface(allFeatures, inventory);
            }

            mHelper.queryInventoryAsync(mGotInventoryListener);
        }
    };

    /**
     * Enable available purchases
     * @param item
     */
    protected void enableSkuInterface(SkuDetails item, Inventory inventory){

        if(item.getSku().equalsIgnoreCase(getResources().getString(R.string.unlockpro))){
            if(!inventory.hasPurchase(item.getSku()))
            {
                //enable text view and button for second sku
                mBtnBuyAllFeatures.setEnabled(true);

                if(item!=null&&item.getPrice()!=null&&item.getPrice().length()>0){
                    mBtnBuyAllFeatures.setEnabled(true);
                }else{
                    mBtnBuyAllFeatures.setEnabled(false);
                }
            }
            else
            {
                //enable text view and button for second sku
                mBtnBuyAllFeatures.setEnabled(true);
            }
            mBtnBuyAllFeatures.setBackgroundResource(R.drawable.greenbutton);

        }
    }


    protected void disableSkuInterfaces(){

        mBtnBuyAllFeatures.setEnabled(false);
        mBtnBuyAllFeatures.setBackgroundResource(R.drawable.disabledbutton);
    }

    private boolean verifyDeveloperPayload(Purchase purchase) {

		/*
		if(payload.equalsIgnoreCase(purchase.getDeveloperPayload())){
			return true;
		}else{
			return false;
		}
		*/
        return true;

    }

    private void setWaitScreen(boolean set) {
        findViewById(R.id.screen_main).setVisibility(set ? View.GONE : View.VISIBLE);
        findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE : View.GONE);
    }

    /**
     * Whenever the application regains focus, the observer is registered again.
     */
    @Override
    public void onStart() {
        super.onStart();
        setWaitScreen(false);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;
        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }
}
