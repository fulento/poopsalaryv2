
package com.poopsalary.activities;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;
import android.support.v4.view.GravityCompat;
import android.widget.AdapterView;
import android.view.View;
import android.support.v4.app.ActionBarDrawerToggle;
import android.content.res.Configuration;
import android.view.MenuItem;

import java.util.ArrayList;

import com.parse.ParseObject;
import com.poopsalary.adapters.NavDrawerAdapter;
import com.poopsalary.fragments.StatsDailyFragment;
import com.poopsalary.fragments.StatsHourlyFragment;
import com.poopsalary.fragments.StatsMonthlyFragment;
import com.poopsalary.fragments.StatsOverviewFragment;
import com.poopsalary.fragments.StatsTotalFragment;
import com.poopsalary.fragments.StatsWeeklyFragment;
import com.poopsalary.fragments.StatsYearlyFragment;
import com.poopsalary.menu.NavDrawerItem;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import com.parse.Parse;
import com.poopsalary.R;

public class StatsMainActivity extends ActionBarActivity
{
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerAdapter adapter;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Fragment mSelectedFrag = null;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    // Fragments
    StatsHourlyFragment mStatsHourlyFragment;
    StatsDailyFragment mStatsDailyFragment;
    StatsWeeklyFragment mStatsWeeklyFragment;
    StatsMonthlyFragment mStatsMonthlyFragment;
    StatsYearlyFragment mStatsYearlyFragment;
    StatsTotalFragment mStatsTotalFragment;
    StatsOverviewFragment mStatsOverviewFragment;

    @Override
    @SuppressWarnings("ResourceType")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initialize app drawer
        setTitle("Stats");
        mTitle = mDrawerTitle = getTitle();

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.stat_menu_array);

        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.stat_menu_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        navDrawerItems = new ArrayList<NavDrawerItem>();


        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));

        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        adapter = new NavDrawerAdapter(getApplicationContext(), navDrawerItems);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);

                if(mSelectedFrag != null)
                {
                    switchToFragment(mSelectedFrag);
                    mSelectedFrag = null;
                }
                //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                //invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // create fragments
        mStatsHourlyFragment = new StatsHourlyFragment();
        mStatsDailyFragment = new StatsDailyFragment();
        mStatsWeeklyFragment = new StatsWeeklyFragment();
        mStatsMonthlyFragment = new StatsMonthlyFragment();
        mStatsYearlyFragment = new StatsYearlyFragment();
        mStatsTotalFragment = new StatsTotalFragment();
        mStatsOverviewFragment = new StatsOverviewFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
                mStatsOverviewFragment).commit();
    }

    // Switch UI to the given fragment
    void switchToFragment(Fragment newFrag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFrag)
                .commit();

    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {

        switch(position)
        {
            case 0:
                //Hourly Stats
                mSelectedFrag = mStatsOverviewFragment;
                setTitle(navMenuTitles[position]);
                break;
            case 1:
                //Daily Stats
                mSelectedFrag = mStatsHourlyFragment;
                setTitle(navMenuTitles[position]);
                break;
            case 2:
                //Weekly Stats
                mSelectedFrag = mStatsDailyFragment;
                setTitle(navMenuTitles[position]);
                break;
            case 3:
                //Monthly Stats
                mSelectedFrag = mStatsMonthlyFragment;
                setTitle(navMenuTitles[position]);
                break;
        }

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);

        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart(){
        super.onStart();

    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onUserLeaveHint() {


    }
}
