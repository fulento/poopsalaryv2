package com.poopsalary.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.poopsalary.R;
import com.poopsalary.old.DatabaseAdapter;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import com.poopsalary.utils.Utilities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SyncOldDataActivity extends Activity {

    DatabaseAdapter dbHelper;
    Cursor mJobsCursor;
    Cursor mLogsCursor;
    List<Jobs> allJobs;
    List<Logs> allLogs;
    boolean showJobs;
    boolean showLogs;
    boolean syncedOldJobs;
    boolean syncedOldLogs;

    TextView mTxtOldJobs;
    TextView mTxtOldLogs;

    Dialog mDialog;

    Jobs newJob;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        setContentView(R.layout.activity_sync_old_data);
        ParseUser.getCurrentUser().fetchInBackground();

        //need to sync data from sql lite
        dbHelper = new DatabaseAdapter(getApplicationContext());
        dbHelper.open();

        mJobsCursor = dbHelper.fetchProfiles();

        allJobs = new ArrayList<Jobs>();
        allLogs = new ArrayList<Logs>();

        mJobsCursor.moveToFirst();
        if (mJobsCursor.getCount() > 0) {

            boolean isFirstItem = true;

            do
            {
                Jobs job = new Jobs();
                job.setJobName(mJobsCursor.getString(mJobsCursor.getColumnIndexOrThrow("profileName")));
                job.setWage(mJobsCursor.getDouble(mJobsCursor.getColumnIndexOrThrow("wage")));
                job.setWageType(mJobsCursor.getString(mJobsCursor.getColumnIndexOrThrow("wageType")));
                job.setACL();
                job.setUser(ParseUser.getCurrentUser());
                job.setIsSelected(false);
                job.setCurrency("USD - United States Dollar");

                if (isFirstItem) {
                    job.setIsSelected(true);
                    isFirstItem = false;
                }

                allJobs.add(job);
            }while(mJobsCursor.moveToNext());
        }

        mTxtOldJobs = (TextView) findViewById(R.id.txtOldJobsQty);
        mTxtOldJobs.setText("Found " + allJobs.size() + " old jobs.");

        showJobs = true;

        mLogsCursor = dbHelper.fetchStatsByDate();
        mLogsCursor.moveToFirst();
        if (mLogsCursor.getCount() > 0) {

            do
            {
                Logs log = new Logs();
                log.setACL();
                log.setUser(ParseUser.getCurrentUser());
                log.setRating(0f);

                Date timeStarted = new Date();
                timeStarted.setTime(mLogsCursor.getLong(mLogsCursor.getColumnIndexOrThrow("dateEntered")) * 1000);
                log.setTimeStarted(timeStarted);

                Date timeCompleted = new Date();
                timeCompleted.setTime(mLogsCursor.getLong(mLogsCursor.getColumnIndexOrThrow("dateEntered")) * 1000 + mLogsCursor.getLong(mLogsCursor.getColumnIndexOrThrow("timeElapsed")));
                log.setTimeCompleted(timeCompleted);

                log.setTotalSeconds(mLogsCursor.getLong(mLogsCursor.getColumnIndexOrThrow("timeElapsed")));
                log.setMoneyEarned(mLogsCursor.getDouble(mLogsCursor.getColumnIndexOrThrow("moneyEarned")));
                log.setIsNew(false);

                log.setHasBeenEdited(false);

                allLogs.add(log);
            }while(mLogsCursor.moveToNext());
        }

        mTxtOldLogs = (TextView) findViewById(R.id.txtOldLogsQty);
        mTxtOldLogs.setText("Found " + allLogs.size() + " old logs.");

        showLogs = false;
        updateUi();

        Button btnSyncJobs = (Button)findViewById(R.id.btnSyncOldJobs);
        Button btnSyncLogs = (Button)findViewById(R.id.btnSyncOldLogs);
        Button btnSkipJobs = (Button)findViewById(R.id.btnJobsSkip);
        Button btnSkipLogs = (Button)findViewById(R.id.btnSkipLogs);

        btnSyncJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(allJobs.size() > 0)
                {
                    showProgressBar("Converting and Saving Jobs...");
                    ParseObject.saveAllInBackground(allJobs, new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            dismissProgressBar();
                            if(e == null)
                            {
                                syncedOldJobs = true;
                                showJobs = false;
                                showLogs = true;
                                updateUi();
                            }
                            else
                            {
                                Utilities.PostExceptionToParse(e);

                                Toast.makeText(mContext, "Jobs are syncing in the background", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        btnSyncLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (allLogs.size() > 0) {
                    ParseQuery<Jobs> jobsQuery = new ParseQuery<Jobs>(Jobs.class);
                    jobsQuery.whereEqualTo("user", ParseUser.getCurrentUser());
                    jobsQuery.whereEqualTo("isSelected", true);

                    showProgressBar("Converting and Saving Logs...");
                    jobsQuery.getFirstInBackground(new GetCallback<Jobs>() {
                        @Override
                        public void done(Jobs job, ParseException e) {

                            if(e != null){
                                Utilities.PostExceptionToParse(e);
                            }

                            if (job == null) {

                                newJob = new Jobs();
                                newJob.setACL();
                                newJob.setCurrency("USD - United States Dollar");
                                newJob.setIsSelected(true);
                                newJob.setUser(ParseUser.getCurrentUser());
                                newJob.setWage(10.0);
                                newJob.setWageType("Hourly");
                                newJob.setJobName("Default Job");
                                newJob.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e != null)
                                        {
                                            Utilities.PostExceptionToParse(e);
                                            Toast.makeText(mContext, "Unable to create new Job", Toast.LENGTH_LONG).show();
                                        }
                                        else
                                        {
                                            newJob.fetchInBackground(new GetCallback<ParseObject>() {
                                                @Override
                                                public void done(ParseObject parseObject, ParseException e) {

                                                    if(e != null){
                                                        Utilities.PostExceptionToParse(e);
                                                    }
                                                    else
                                                    {
                                                        for(int i = 0; i < allLogs.size(); i++)
                                                        {
                                                            allLogs.get(i).setJob(newJob);
                                                        }

                                                        ParseObject.saveAllInBackground(allLogs, new SaveCallback() {
                                                            @Override
                                                            public void done(ParseException e) {
                                                                if(e != null)
                                                                {
                                                                    Utilities.PostExceptionToParse(e);
                                                                    Toast.makeText(mContext, "Unable to save Logs", Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        });
                                                    }


                                                }
                                            });
                                        }

                                    }
                                });
                            }
                            else
                            {

                                for(int i = 0; i < allLogs.size(); i++)
                                {
                                    allLogs.get(i).setJob(job);
                                }

                                if(allLogs.size() > 0)
                                {
                                    ParseObject.saveAllInBackground(allLogs, new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            dismissProgressBar();

                                            if(e == null)
                                            {
                                                setResult(RESULT_OK);
                                                ParseUser.getCurrentUser().put("hasSyncedOriginalData", true);
                                                ParseUser.getCurrentUser().saveEventually();
                                                finish();

                                            }
                                            else
                                            {
                                                Utilities.PostExceptionToParse(e);
                                                Toast.makeText(mContext, "Logs are being synced in the background. Carry on.", Toast.LENGTH_LONG).show();
                                            }

                                        }
                                    });
                                }

                            }

                        }
                    });

                }
                else
                {
                    setResult(RESULT_OK);
                    ParseUser.getCurrentUser().put("hasSyncedOriginalData", true);
                    ParseUser.getCurrentUser().saveEventually();
                    finish();
                }
            }
        });

        btnSkipJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                syncedOldJobs = false;
                showLogs = true;
                showJobs = false;
                updateUi();
            }
        });

        btnSkipLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setResult(RESULT_OK);
                ParseUser.getCurrentUser().put("hasSyncedOriginalData", true);
                ParseUser.getCurrentUser().saveEventually();
                finish();
            }
        });
    }

    void updateUi() {
        findViewById(R.id.layoutJobsSync).setVisibility(showJobs ? View.VISIBLE : View.GONE);
        findViewById(R.id.layoutLogsSync).setVisibility(showLogs ? View.VISIBLE : View.GONE);

        TextView explanation = (TextView)findViewById(R.id.txtExplanation);

        if(showJobs)
        {
            explanation.setText("Syncing jobs from the previous version of the app will allow you to keep all your progress with all your wages and wage types set up. Syncing will also back up jobs online for use on multiple devices");
        }

        if(showLogs)
        {
            explanation.setText("Syncing logs (stats) from the previous version of the app will allow you to keep all your deposits and convert them to the new data format for better stat tracking. Syncing will also back up all your stats online for use on multiple devices");
        }
    }

    public void showProgressBar(String msg){
        mDialog = ProgressDialog.show(this, "", msg, true);
    }

    public void dismissProgressBar(){
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dbHelper.close();
    }
}
