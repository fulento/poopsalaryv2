
package com.poopsalary.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;
import android.widget.ListView;
import android.support.v4.view.GravityCompat;
import android.widget.AdapterView;
import android.view.View;
import android.support.v4.app.ActionBarDrawerToggle;
import android.content.res.Configuration;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.poopsalary.R;
import com.poopsalary.adapters.NavDrawerAdapter;
import com.poopsalary.fragments.MonitorFragment;
import com.poopsalary.fragments.ParseDepositsFragment;
import com.poopsalary.fragments.ParseJobsFragment;
import com.poopsalary.fragments.SettingsProfilesFragment;
import com.poopsalary.menu.NavDrawerItem;
import com.poopsalary.models.ParseExceptions;
import com.poopsalary.models.ExchangeRates;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import com.poopsalary.utils.AppPreferences;
import com.parse.ParseUser;
import com.parse.Parse;
import com.poopsalary.utils.Utilities;

public class MainActivity extends BaseGameActivity
        implements MonitorFragment.Listener,
            ParseDepositsFragment.Listener, SettingsProfilesFragment.Listener, ParseJobsFragment.Listener {

    private Dialog mDialog;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerAdapter adapter;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Fragment mSelectedFrag = null;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    // Fragments
    MonitorFragment mMonitorFragment;
    ParseDepositsFragment mDepositsFragment;
    SettingsProfilesFragment mSettingsProfilesFragment;
    ParseJobsFragment mJobsFragment;

    boolean mUpdatePoopsRated = false;
    boolean mUpdatePoopsShared = false;
    boolean mUpdateAppShared = false;
    boolean mUpdatePoopDeposited = false;
    int mAchievementSeconds = 0;
    double mAchievementMoney = 0;

    String menuAction = "";

    // request codes we use when invoking an external activity
    final int RC_RESOLVE = 5000, RC_UNUSED = 5001;

    // tag for debug logging
    final boolean ENABLE_DEBUG = true;
    final String TAG = "TanC";

    @Override
    @SuppressWarnings("ResourceType")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedClients(CLIENT_ALL);

        mTitle = mDrawerTitle = getTitle();

        navMenuTitles = getResources().getStringArray(R.array.menu_array);
        navMenuIcons = getResources().obtainTypedArray(R.array.menu_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        ParseObject.registerSubclass(Logs.class);
        ParseObject.registerSubclass(Jobs.class);
        ParseObject.registerSubclass(ExchangeRates.class);
        ParseObject.registerSubclass(ParseExceptions.class);
        //Parse.initialize(this, "2xnh1xMIEwErXd1xgo73uy52y8QXSsQxjLauLGuf", "Tw7MILkNfLTNTasUT32AOOMBQUYN5eJoEu6TCjlg");
        try {
            Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                    .applicationId("2xnh1xMIEwErXd1xgo73uy52y8QXSsQxjLauLGuf")
                    .clientKey("Tw7MILkNfLTNTasUT32AOOMBQUYN5eJoEu6TCjlg")
                    .enableLocalDataStore()
                    .server("https://parseapi.back4app.com")
                    .build()
            );
            ParseUser.enableRevocableSessionInBackground();
        }
        catch (Exception ex) {

        }


        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1), true));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), true));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons.getResourceId(9, -1), true));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons.getResourceId(10, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[11], navMenuIcons.getResourceId(11, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[12], navMenuIcons.getResourceId(12, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[13], navMenuIcons.getResourceId(13, -1)));

        navMenuIcons.recycle();

        adapter = new NavDrawerAdapter(getApplicationContext(),
                navDrawerItems);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open,  R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);

                if(mSelectedFrag != null)
                {
                    switchToFragment(mSelectedFrag);
                    mSelectedFrag = null;
                }
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // create fragments
        mMonitorFragment = new MonitorFragment();
        mDepositsFragment = new ParseDepositsFragment();
        mSettingsProfilesFragment = new SettingsProfilesFragment();
        mJobsFragment = new ParseJobsFragment();

        // listen to fragment events
        mMonitorFragment.setListener(this);
        mDepositsFragment.setListener(this);
        mSettingsProfilesFragment.setListener(this);
        mJobsFragment.setListener(this);

        if(ParseUser.getCurrentUser() == null){
            //first time run setting fragment
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
                    mSettingsProfilesFragment).commit();
        }
        else {
            ParseUser.getCurrentUser().fetchInBackground();
            // add initial fragment (welcome fragment)
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
                    mMonitorFragment).commit();
        }

        //TODO implement I have no Life achievement
        //TODO: implement offline achievement storage
        //TODO: remove this for prod

        //check currency rates online
        if(Utilities.getDaysDiffHours(new Date().getTime(), AppPreferences.getLastRateCheck(getApplication())) > 24)
        {
            showProgressBar("Downloading Exchange Rates...");
            ParseQuery<ExchangeRates> query = new ParseQuery<ExchangeRates>(ExchangeRates.class);
            query.findInBackground(new FindCallback<ExchangeRates>() {
                @Override
                public void done(List<ExchangeRates> exchangeRates, ParseException e) {
                    dismissProgressBar();
                    ParseObject.fetchAllInBackground(exchangeRates);
                    AppPreferences.setLastRateCheck(getApplication(), new Date().getTime());

                }
            });
        }
    }

    // Switch UI to the given fragment
    void switchToFragment(Fragment newFrag) {
        if(ParseUser.getCurrentUser() != null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFrag)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mSettingsProfilesFragment)
                    .commit();
        }
    }

    @Override
    public void onDepositRecorded(int seconds, double earned) {
        if(isSignedIn())
        {
            updatePoopsDeposited();
            mAchievementMoney = 0;
            mAchievementSeconds = 0;
            mUpdatePoopDeposited = false;
        }
        else
        {
            mUpdatePoopDeposited = true;
            mAchievementSeconds = seconds;
            mAchievementMoney = earned;
        }

    }

    @Override
    public void onPoopRated()
    {
        if(isSignedIn())
        {
            updatePoopRated();
            mUpdatePoopsRated = false;
        }
        else
        {
            mUpdatePoopsRated = true;
            if(ParseUser.getCurrentUser().get("hasGoogleAccount").equals(true))
            {
                setContext(this);
                beginUserInitiatedSignIn();
            }

        }

    }

    @Override
    public void onSharePoop()
    {
        if(isSignedIn())
        {
            updateSharedPoop();
            mUpdatePoopsShared = false;
        }
        else
        {
            mUpdatePoopsShared = true;
            if(ParseUser.getCurrentUser().get("hasGoogleAccount").equals(true))
            {
                setContext(this);
                beginUserInitiatedSignIn();
            }
        }

    }

    public void onShareApp()
    {
        if(isSignedIn())
        {
            updateAppsShared();
            mUpdateAppShared = false;
        }
        else
        {
            mUpdateAppShared = true;
            if(ParseUser.getCurrentUser().get("hasGoogleAccount").equals(true))
            {
                setContext(this);
                beginUserInitiatedSignIn();
            }
        }

    }

    private void updatePoopsDeposited(){
        Games.Leaderboards.submitScore(getApiClient(), getString(R.string.leaderboard_longest_poop), mAchievementSeconds * 1000);

        //Increment Achievements
        int minutes = mAchievementSeconds % 60;

        if(minutes > 0)
        {
            //Record First Poop
            Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_turtle_poking));

            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_intern), minutes);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_parttime_job), minutes);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_fulltime_job), minutes);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_i_own_this_place), minutes);

            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_bowel_movement_beginner), 1);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_professor_of_plop), 1);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_doctor_of_dung), 1);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_king_of_caca), 1);

            if(minutes >=10){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_postlunch_special));
            }

            if(minutes >= 20){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_stall_hog));
            }

            if(minutes >= 30){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_dead_legs));
            }

            if(minutes >= 60){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_medic));
            }

            ParseQuery<Logs> query = new ParseQuery<Logs>(Logs.class);
            query.fromLocalDatastore();
            query.findInBackground(new FindCallback<Logs>() {
                @Override
                public void done(List<Logs> logs, ParseException e) {
                    Games.Leaderboards.submitScore(getApiClient(), getString(R.string.leaderboard_total_poops), logs.size());
                }
            });

        }
        else
        {
            //Record poop < 1 min
            Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_snip_it_off));
        }

        int moneyEarned = (int)Math.round(mAchievementMoney);

        if(moneyEarned > 0)
        {
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_benjamins_in_the_bathroom), moneyEarned);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_4_oh_1k), moneyEarned);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_investment_banker), moneyEarned);
            Games.Achievements.increment(getApiClient(), getString(R.string.achievement_make_it_rain), moneyEarned);

            if(moneyEarned >= 2){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_minimum_wage));
            }

            if(moneyEarned >= 15){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_i_got_a_real_job));
            }

            if(moneyEarned >= 30){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_im_a_man___im_40));
            }

            if(moneyEarned >= 100){
                Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_mogul));
            }
        }

        ParseQuery<Logs> logsQuery = new ParseQuery<Logs>(Logs.class);
        logsQuery.fromLocalDatastore();
        logsQuery.orderByDescending("timeCompleted");
        logsQuery.findInBackground(new FindCallback<Logs>() {
            @Override
            public void done(List<Logs> logs, ParseException e) {
                Games.Leaderboards.submitScore(getApiClient(), getString(R.string.leaderboard_total_poops), logs.size());

                //TODO: Update 5 day streak achievements R.string.achievement_mr_consistency
                //TODO: Update 2-a-day achievement
                //TODO: Implement 2 poops on different jobs same day
            }
        });

    }

    private void updatePoopRated()
    {
        //Rate First Poop
        Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_quick_study));

        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_student_of_the_game), 1);
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_no_sht_sherlock), 1);
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_feces_forensic), 1);
    }

    public void updateSharedPoop(){
        //first share
        Games.Achievements.unlock(getApiClient(), getString(R.string.achievement_t_m_i_));

        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_no_shame), 1);
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_proud_pooper), 1);
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_exhibitor_of_excrement), 1);
    }

    private void updateAppsShared(){
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_yes_this_app_exists), 1);
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_youre_so_friendly), 1);
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_social_butterfly), 1);
        Games.Achievements.increment(getApiClient(), getString(R.string.achievement_is_your_last_name_zuckerburg), 1);
    }

    @Override
    public void onSignInFailed() {
        // Sign-in failed, so show sign-in button on main menu
        mSettingsProfilesFragment.setShowSignInButton(true);
    }

    @Override
    public void onSignInSucceeded() {
        //From google login
        // Show sign-out button on main menu
        mSettingsProfilesFragment.setShowSignInButton(false);

        // Set the greeting appropriately on main menu
        Player p = Games.Players.getCurrentPlayer(getApiClient());

        AppPreferences.setUserPayLoad(this, p.getPlayerId());

        String displayName;
        if (p == null) {
            displayName = "???";
        } else {
            displayName = p.getDisplayName();
        }

        if(ParseUser.getCurrentUser().get("googleId") != null)
        {
            if(ParseUser.getCurrentUser().get("googleId").toString().length() == 0 || ParseUser.getCurrentUser().get("googleId") == "undefined")
            {
                //ParseUser hasn't been linked to googleId yet. set it up
                ParseUser.getCurrentUser().put("googleId", p.getPlayerId());
                ParseUser.getCurrentUser().put("hasGoogleAccount", true);
                ParseUser.getCurrentUser().saveEventually();
            }
            else
            {
                if(ParseUser.getCurrentUser().get("googleId").equals(p.getPlayerId())) {
                    ParseUser.getCurrentUser().put("googleSignedIn", true);
                    ParseUser.getCurrentUser().put("hasGoogleAccount", true);
                    ParseUser.getCurrentUser().saveEventually();
                } else {
                    //logged in google user doesn't match ParseUser's google Id.
                    signOut();
                    Toast.makeText(getApplicationContext(), "Google user doesn't match linked ParseUser account", Toast.LENGTH_LONG).show();
                }
            }
        }


        AppPreferences.setHasLinkedGoogleAccount(this, true);

        if(menuAction != null)
        {
            if(menuAction.length() > 0)
            {
                if(menuAction == "Achievements")
                {
                    openAchievements();
                }
                else if(menuAction == "Leaderboards")
                {
                    openLeaderboards();
                }
            }

            menuAction = "";
        }
        if(mUpdatePoopsRated){
            //update rating achievements
            updatePoopRated();
            mUpdatePoopsRated = false;
        }

        if(mUpdatePoopDeposited){
            updatePoopsDeposited();
            mAchievementMoney = 0;
            mAchievementSeconds = 0;
            mUpdatePoopDeposited = false;
        }

        if(mUpdatePoopsShared){
            updateSharedPoop();
            mUpdatePoopsShared = false;
        }

        if(mUpdateAppShared){
            updateAppsShared();
            mUpdateAppShared = false;
        }

        //TODO: implement offline saving of achievements once we're signed in
    }

    @Override
    public void onSignInButtonClicked() {
        // start the sign-in flow
        setContext(this);
        beginUserInitiatedSignIn();
    }

    @Override
    public void onSignOutButtonClicked() {
        signOut();
        ParseUser.getCurrentUser().put("googleSignedIn", false);
        ParseUser.getCurrentUser().saveEventually();
        mSettingsProfilesFragment.setShowSignInButton(true);
        AppPreferences.setHasLinkedGoogleAccount(this, false);
    }

    @Override
    public void onSignOutParseButtonClicked()
    {
        ParseUser.getCurrentUser().put("googleSignedIn", false);
        ParseUser.getCurrentUser().saveEventually();
        ParseUser.logOut();
        signOut();

        mSettingsProfilesFragment.setShowSignInButton(true);
        mSettingsProfilesFragment.setShowSigninParseButton(true);

    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {

        switch(position)
        {
            case 0:
                //do nothing. header
                break;
            case 1:
                //switchToFragment(mStatsHourlyFragment);
                mSelectedFrag = mMonitorFragment;
                setTitle(navMenuTitles[position]);
                break;
            case 2:
                //switchToFragment(mStatsDailyFragment);
                mSelectedFrag = mDepositsFragment;
                setTitle(navMenuTitles[position]);
                break;
            case 3:
                //switchToFragment(mStatsMonthlyFragment);
                Intent statIntent = new Intent(this, StatsMainActivity.class);
                startActivity(statIntent,null);
                break;
            case 4:
                //switchToFragment(mStatsWeeklyFragment);
                mSelectedFrag = mJobsFragment;
                setTitle(navMenuTitles[position]);
                break;
            case 5:
                mSelectedFrag = mSettingsProfilesFragment;
                setTitle(navMenuTitles[position]);
                break;
            case 6:
                //do nothing. header
                break;
            case 7:
                if(ParseUser.getCurrentUser() != null)
                {
                    if (isSignedIn()) {
                        openAchievements();

                    } else {
                        menuAction = "Achievements";
                        setContext(this);
                        beginUserInitiatedSignIn();
                    }
                }
                else
                {
                    mSelectedFrag = mSettingsProfilesFragment;
                }

                break;
            case 8:
                if(ParseUser.getCurrentUser() != null)
                {
                    if (isSignedIn()) {
                        openLeaderboards();

                    } else {
                        menuAction = "Leaderboards";
                        setContext(this);
                        beginUserInitiatedSignIn();
                    }
                }
                else
                {
                    mSelectedFrag = mSettingsProfilesFragment;
                }

                break;
            case 9:
                //do nothing header
                break;
            case 10:
                //facebook
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/230749203651099"));
                    startActivity(intent);
                } catch(Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/pooponthejob")));
                }
                break;
            case 11:
                //twitter
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("twitter://user?user_id=399584436"));
                    startActivity(intent);

                }catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://twitter.com/#!/poopsalary")));
                }
                break;
            case 12:
                //gplus
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/115267169639612367044/posts")));
                break;
            case 13:
                //share us
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out Poop Salary");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check out this app! #PoopSalary http://goo.gl/GNkAkg");
                startActivity(Intent.createChooser(sharingIntent, "Share Poop Salary"));
                break;
        }

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);

        mDrawerLayout.closeDrawer(mDrawerList);
    }

    private void openAchievements()
    {
        startActivityForResult(Games.Achievements.getAchievementsIntent(getApiClient()), RC_UNUSED);
    }

    private void openLeaderboards()
    {
        startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(getApiClient()), RC_UNUSED);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        else
        {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSignInProcessOver()
    {
        switchToFragment(mSettingsProfilesFragment);
    }
    public void showProgressBar(String msg){
        mDialog = ProgressDialog.show(getApplicationContext(), "", msg, true);
    }

    public void dismissProgressBar(){
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    @Override
    public void onStart(){
        super.onStart();

        if(ParseUser.getCurrentUser() != null){
            if(ParseUser.getCurrentUser().get("hasGoogleAccount").equals(true)){
                if(!isSignedIn()){
                    if(!ParseUser.getCurrentUser().get("googleSignedIn").equals(true))
                    {
                        setContext(this);
                        beginUserInitiatedSignIn();
                    }
                }
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        if(ParseUser.getCurrentUser() != null){
            if(ParseUser.getCurrentUser().get("hasGoogleAccount").equals(true)){
                if(!isSignedIn()){
                    if(!AppPreferences.getIsFirstRun(this))
                    {
                        setContext(this);
                        getGameHelper().reconnectClient();
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // mGameHelper.onActivityResult(requestCode, requestCode, data); // before - broken
        getGameHelper().onActivityResult(requestCode, resultCode, data); // after - working! \o/


        /*if(resultCode != RESULT_OK){
            Toast.makeText(this, "Premium features not enabled...",  Toast.LENGTH_SHORT).show();
            signOut();
        }*/
    }

    @Override
    public void onUserLeaveHint() {
        //TODO: refresh local data store for next run

    }
}
