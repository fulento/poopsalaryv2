package com.poopsalary.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.content.DialogInterface;

import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.poopsalary.R;
import com.poopsalary.models.Logs;
import com.poopsalary.utils.Utilities;

public class DepositReceiptActivity extends Activity {

    String mStartedOn;
    String mEndedOn;
    String mEarned;
    String mElapsed;
    float mOriginalRating;
    float mRating;

    TextView txtStartedOn;
    TextView txtEndedOn;
    TextView txtEarned;
    TextView txtElapsed;
    RatingBar rbRating;

    boolean mPoopShared = false;

    Logs mLog = new Logs();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_deposit_receipt);

        txtStartedOn = (TextView)findViewById(R.id.txtDateStarted);
        txtEndedOn = (TextView)findViewById(R.id.txtDateEnded);
        txtEarned = (TextView)findViewById(R.id.txtEarned);
        txtElapsed = (TextView)findViewById(R.id.txtElapsed);
        rbRating = (RatingBar)findViewById(R.id.ratingBarEdit);

        ImageButton btnShare = (ImageButton)findViewById(R.id.btnSharePoop);
        ImageButton btnDelete = (ImageButton)findViewById(R.id.btnCancelPoop);
        ImageButton btnClose = (ImageButton)findViewById(R.id.btnAcceptPoop);

        Intent intent = getIntent();

        if(intent.getStringExtra("logId") != null)
        {
            ParseQuery<Logs> query = new ParseQuery<Logs>(Logs.class);
            query.fromLocalDatastore();
            query.getInBackground(intent.getStringExtra("logId"), new GetCallback<Logs>() {
                @Override
                public void done(Logs logs, ParseException e) {
                    mLog = logs;

                    if(mLog != null)
                    {
                        mStartedOn = Utilities.getFullFormattedDate(mLog.getTimeStarted().getTime());
                        mEndedOn = Utilities.getFullFormattedDate(mLog.getTimeCompleted().getTime());
                        mEarned = Utilities.ConvertWageToLocale(getApplicationContext(), mLog.getMoneyEarned());
                        mElapsed = Utilities.MillisecondsToFormattedElapsedTime(mLog.getTotalSeconds() * 1000);
                        mRating = mOriginalRating = mLog.getRating();
                    }

                    txtStartedOn.setText(mStartedOn);
                    txtEndedOn.setText(mEndedOn);
                    txtEarned.setText(mEarned);
                    txtElapsed.setText(mElapsed);
                    rbRating.setRating(mRating);
                }
            });
        }
        else
        {
            ParseQuery<Logs> query = new ParseQuery<Logs>(Logs.class);
            query.fromLocalDatastore();
            query.orderByDescending("timeCompleted");
            query.getFirstInBackground(new GetCallback<Logs>() {
                @Override
                public void done(Logs logs, ParseException e) {
                    mLog = logs;

                    if(mLog != null)
                    {
                        mStartedOn = Utilities.getFullFormattedDate(mLog.getTimeStarted().getTime());
                        mEndedOn = Utilities.getFullFormattedDate(mLog.getTimeCompleted().getTime());
                        mEarned = Utilities.ConvertWageToLocale(getApplicationContext(), mLog.getMoneyEarned());
                        mElapsed = Utilities.MillisecondsToFormattedElapsedTime(mLog.getTotalSeconds() * 1000);
                        mRating = mOriginalRating = mLog.getRating();
                    }

                    txtStartedOn.setText(mStartedOn);
                    txtEndedOn.setText(mEndedOn);
                    txtEarned.setText(mEarned);
                    txtElapsed.setText(mElapsed);
                    rbRating.setRating(mRating);
                }
            });
        }

        rbRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                mLog.setRating(rating);
                mRating = rating;
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPoopShared = true;
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Guess What?!");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I made " + mEarned + " pooping at work with #PoopSalary goo.gl/GNkAkg");
                startActivity(Intent.createChooser(sharingIntent, "Share Poop"));
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DepositReceiptActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Confirm Delete...");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want delete this?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.ic_action_discard);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        Toast.makeText(getApplicationContext(), "Flushing log", Toast.LENGTH_SHORT).show();
                        // Write your code here to invoke YES event
                        mLog.deleteEventually();
                        Intent intent = new Intent ();
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }
        });

       btnClose.setOnClickListener(new View.OnClickListener() {
           @Override
       public void onClick(View view)
       {
           Intent intent = new Intent ();

           if(mOriginalRating == 0 && mRating > 0){

               intent.putExtra ("ratingUpdated", true);

           }

           if(mLog.getIsNew())
           {
                intent.putExtra("poopIsNew", true);
                intent.putExtra("secondsPooped", mLog.getTotalSeconds());
                intent.putExtra("moneyEarned", mLog.getMoneyEarned());
           }

           if(mPoopShared)
           {
               intent.putExtra("sharedPoop", true);
           }

           setResult(Activity.RESULT_OK, intent);

           mLog.setIsNew(false);
           mLog.saveEventually();
           finish();

           }
       });

    }


    @Override
    public void onDestroy(){
        super.onDestroy();
    }

}