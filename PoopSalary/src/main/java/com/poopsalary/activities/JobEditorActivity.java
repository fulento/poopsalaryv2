package com.poopsalary.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.poopsalary.R;
import com.poopsalary.models.ExchangeRates;
import com.poopsalary.models.Jobs;
import com.poopsalary.models.Logs;
import com.poopsalary.utils.AppPreferences;

import java.util.ArrayList;
import java.util.List;

public class JobEditorActivity extends Activity {

    EditText mProfileNameEdit;
    EditText mWageEdit;
    String mCurrency;
    Spinner mSpinWageTypeEdit;
    Spinner mSpinCurrency;
    CheckBox mChkActive;
    String mWageType;
    Jobs mJob;
    boolean mIsNewJob = false;
    List<Logs> mLogsToDelete = new ArrayList<Logs>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_job_editor);

        mProfileNameEdit = (EditText)findViewById(R.id.txtProfileNameEdit);
        mWageEdit = (EditText)findViewById(R.id.txtWageEdit);
        mSpinWageTypeEdit = (Spinner)findViewById(R.id.spinWageTypeEdit);
        mSpinCurrency = (Spinner)findViewById(R.id.spinSelectCurrency);
        mChkActive = (CheckBox)findViewById(R.id.cbIsActiveEdit);

        ImageButton btnSave = (ImageButton)findViewById(R.id.btnSaveJob);
        ImageButton btnDelete = (ImageButton) findViewById(R.id.btnDeleteJob);

        Intent intent = getIntent();

        mProfileNameEdit.setInputType(InputType.TYPE_CLASS_TEXT);
        mProfileNameEdit.setImeOptions(EditorInfo.IME_ACTION_NEXT | EditorInfo.TYPE_TEXT_FLAG_CAP_WORDS);

        mWageEdit.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        mWageEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);

        if(intent.getStringExtra("jobId") != null)
        {
            //Edit Job
            ParseQuery<Jobs> query = new ParseQuery<Jobs>(Jobs.class);
            query.fromLocalDatastore();
            query.getInBackground(intent.getStringExtra("jobId"), new GetCallback<Jobs>() {
                @Override
                public void done(Jobs jobs, ParseException e) {
                    mJob = jobs;
                    mProfileNameEdit.setText(mJob.getJobName());
                    mWageEdit.setText(Double.toString(mJob.getWage()));
                    if(mJob.getIsSelected()){
                        mChkActive.setChecked(true);
                    } else {
                        mChkActive.setChecked(false);
                    }

                    mWageType = mJob.getWageType();
                    ArrayAdapter<CharSequence> wageAdapter = setSpinnerAdapter(mSpinWageTypeEdit);
                    mSpinWageTypeEdit.setAdapter(wageAdapter);
                    mSpinWageTypeEdit.setSelection(wageAdapter.getPosition(mWageType));

                    mCurrency = mJob.getCurrency();
                    ArrayAdapter<CharSequence> currencyAdapter = setCurrencyAdapter(mSpinCurrency);
                    mSpinCurrency.setAdapter(currencyAdapter);
                    mSpinCurrency.setSelection(currencyAdapter.getPosition(mCurrency));

                }
            });
        }
        else
        {
            mIsNewJob = true;

            //New Job
            mJob = new Jobs();

            //set default name
            mJob.setJobName("New Job");
            mProfileNameEdit.setText(mJob.getJobName());

            //set default wage
            mJob.setWage(10.0);
            mWageEdit.setText(Double.toString(mJob.getWage()));

            //set default selection status
            mJob.setIsSelected(false);
            if(mJob.getIsSelected()){
                mChkActive.setChecked(true);
            } else {
                mChkActive.setChecked(false);
            }

            //set default wage type
            mJob.setWageType("Hourly");
            mWageType = mJob.getWageType();


            ArrayAdapter<CharSequence> wageAdapter = setSpinnerAdapter(mSpinWageTypeEdit);
            mSpinWageTypeEdit.setAdapter(wageAdapter);
            mSpinWageTypeEdit.setSelection(wageAdapter.getPosition(mWageType));

            //set default currency
            mJob.setCurrency("USD - United States Dollar");
            mCurrency = mJob.getCurrency();

            ArrayAdapter<CharSequence> currencyAdapter = setCurrencyAdapter(mSpinCurrency);
            mSpinCurrency.setAdapter(currencyAdapter);
            mSpinCurrency.setSelection(currencyAdapter.getPosition(mCurrency));

            mJob.setUser(ParseUser.getCurrentUser());
            mJob.setACL();
        }

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ParseQuery<Logs> logsQuery = new ParseQuery<Logs>(Logs.class);
                logsQuery.whereEqualTo("job", mJob);
                logsQuery.fromLocalDatastore();
                logsQuery.findInBackground(new FindCallback<Logs>() {
                    @Override
                    public void done(List<Logs> logs, ParseException e) {

                        mLogsToDelete = logs;

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(JobEditorActivity.this);

                        // Setting Dialog Title
                        alertDialog.setTitle("Delete Job and Logs...");

                        // Setting Dialog Message
                        alertDialog.setMessage("This Job has " + logs.size() + " Logs attached to it. Delete Jobs and Logs?");

                        // Setting Icon to Dialog
                        alertDialog.setIcon(R.drawable.ic_action_discard);

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "Flushing log", Toast.LENGTH_SHORT).show();
                                // Write your code here to invoke YES event

                                for (int i = 0; i < mLogsToDelete.size(); i++) {
                                    mLogsToDelete.get(i).deleteEventually();
                                }
                                mJob.deleteEventually();

                                setResult(8888);
                                finish();
                            }
                        });

                        // Setting Negative "NO" Button
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
                    }
                });
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                Intent intent = new Intent ();
                setResult(Activity.RESULT_OK, intent);

                ContentValues updateProfile = new ContentValues();

                mJob.setJobName(mProfileNameEdit.getText().toString());
                mJob.setWage(Double.parseDouble(mWageEdit.getText().toString()));
                mJob.setWageType(mWageType.toString());
                mJob.setIsSelected((mChkActive.isChecked()) ? true : false);
                mJob.setCurrency(mCurrency.toString());

                if(mIsNewJob) {
                    mJob.pinInBackground();
                    mJob.saveEventually();
                }

                if(mJob.getIsSelected())
                {
                    ParseQuery<ExchangeRates> query = new ParseQuery<ExchangeRates>(ExchangeRates.class);
                    query.fromLocalDatastore();
                    query.whereEqualTo("symbol", mCurrency.substring(0,3));
                    query.getFirstInBackground(new GetCallback<ExchangeRates>() {
                        @Override
                        public void done(ExchangeRates exchangeRate, ParseException e) {
                            if(e == null)
                            {
                                AppPreferences.setConversionRate(getApplication(), exchangeRate.getRate());
                            }
                            else
                            {
                                Log.d("currency", e.getMessage());
                            }

                        }
                    });

                    //current job is set as selected. mark all other jobs as unselected
                    ParseQuery<Jobs> jobsQuery = new ParseQuery<Jobs>(Jobs.class);
                    jobsQuery.fromLocalDatastore();
                    jobsQuery.whereEqualTo("user", ParseUser.getCurrentUser());
                    jobsQuery.findInBackground(new FindCallback<Jobs>() {
                        @Override
                        public void done(List<Jobs> jobs, ParseException e) {

                            if(e == null)
                            {
                                for(int i = 0; i < jobs.size(); i++) {
                                    jobs.get(i).setIsSelected(false);
                                    jobs.get(i).saveEventually();
                                }

                                mJob.setIsSelected(true);
                                mJob.saveEventually();
                                setResult(9999);
                                finish();
                            }
                            else
                            {
                                mJob.setIsSelected(true);
                                mJob.saveEventually();
                                setResult(9999);
                                finish();
                            }

                        }
                    });
                }
                else
                {
                    mJob.saveEventually();
                    setResult(9999);
                    finish();
                }

            }
        });

    }

    private ArrayAdapter<CharSequence> setSpinnerAdapter(Spinner spinner)
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.wage_types, R.layout.row_spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String selected = parent.getItemAtPosition(pos).toString();
                mWageType = selected;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        return adapter;
    }

    private ArrayAdapter<CharSequence> setCurrencyAdapter(Spinner spinner)
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.currency_values, R.layout.row_spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String selected = parent.getItemAtPosition(pos).toString();
                mCurrency = selected;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        return adapter;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

    }

}
